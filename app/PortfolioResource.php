<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

/**
 * Description of PortfolioResource
 *
 * @author seth
 */
use Illuminate\Database\Eloquent\Model;

class PortfolioResource extends Model{
    //put your code here
    protected $table = 'portfolio_resource';
    
    public function portfolio(){
        return $this->belongsTo('App\Portfolio' , 'portfolio', 'id');
    }
}
