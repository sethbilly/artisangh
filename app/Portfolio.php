<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
/**
 * Description of Portfolio
 *
 * @author seth
 */
class Portfolio extends Model{
    //put your code here
    protected $table = 'portfolio';
    
    public function portfolioImages(){
        return $this->hasMany('App\PortfolioResource', 'portfolio', 'id');
    }
}
