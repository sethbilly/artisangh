<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class ArtisanCategory extends Model{
    protected $table = 'artisan_category';
    
    public function members(){
        return $this->hasMany('App\Member', 'skill_having', 'id');
    }
}
