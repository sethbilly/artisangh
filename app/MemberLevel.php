<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class MemberLevel extends Model{
    protected $table = 'member_level';
    
    public function members(){
        return $this->hasMany('App\Member', 'level', 'id');
    }
}
