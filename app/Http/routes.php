<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

/*
  |
  |Basic routes for the public views come here
  |
 */
Route::get('/', ['as' => 'home', 'uses' => 'IndexPageController@home']);
Route::get('faqs', ['as' => 'faqs', 'uses' => 'IndexPageController@faqs'
]);
Route::get('partner-us', ['as' => 'partner-us', 'uses' => 'IndexPageController@partnerUs'
]);
//Route group for about-us
Route::group(['prefix' => 'about-us'], function() {
    Route::get('who-we-are', ['as' => 'who-we-are', 'uses' => 'IndexPageController@whoWeAre'
    ]);
    Route::get('mission', ['as' => 'mission', 'uses' => 'IndexPageController@mission'
    ]);
    Route::get('vision', ['as' => 'vision', 'uses' => 'IndexPageController@vision'
    ]);
    Route::get('objectives', ['as' => 'objectives', 'uses' => 'IndexPageController@objectives'
    ]);
    Route::get('contact-us', ['as' => 'contact-us', 'uses' => 'IndexPageController@contactUs'
    ]);
});
//Route group for services
Route::group(['prefix' => 'services'], function() {
    Route::get('certification', ['as' => 'certification', 'uses' => 'IndexPageController@certification'
    ]);
    Route::get('job-placement', ['as' => 'job-placement', 'uses' => 'IndexPageController@jobPlacement'
    ]);
    Route::get('training', ['as' => 'training', 'uses' => 'IndexPageController@training'
    ]);
    Route::get('assistance', ['as' => 'assistance', 'uses' => 'IndexPageController@assistance'
    ]);
});

Route::group(['prefix' => 'partners'], function() {
    Route::get('aabn', ['as' => 'aabn', 'uses' => 'IndexPageController@aabn'
    ]);
    Route::get('global-communities', ['as' => 'global-communities', 'uses' => 'IndexPageController@globalCommunities'
    ]);
    Route::get('hfc-boafo', ['as' => 'hfc-boafo', 'uses' => 'IndexPageController@hfcBoafo'
    ]);
    Route::get('yes-gh', ['as' => 'yes-gh', 'uses' => 'IndexPageController@yesGh'
    ]);
    Route::get('ocig', ['as' => 'ocig', 'uses' => 'IndexPageController@ocig'
    ]);
    Route::get('cotvet', ['as' => 'cotvet', 'uses' => 'IndexPageController@cotvet'
    ]);
    Route::get('nvti', ['as' => 'nvti', 'uses' => 'IndexPageController@nvti'
    ]);
});

//Route group for programs
Route::group(['prefix' => 'programs'], function() {
    Route::get('yedie', ['as' => 'yedie', 'uses' => 'IndexPageController@yedie'
    ]);
});

//Route group for mediacenter
Route::group(['prefix' => 'media-center'], function() {
    Route::get('gallery', ['as' => 'gallery', 'uses' => 'IndexPageController@gallery'
    ]);
    Route::get('news-events', ['as' => 'news-events', 'uses' => 'IndexPageController@newsAndEvents'
    ]);
});

//Routes to the top menu links
Route::get('register', ['as' => 'register', 'uses' => 'RegistrationController@register'
]);
Route::post('register-new', ['as' => 'register-new', 'uses' => 'RegistrationController@registerMember'
]);
Route::get('register-successful/{checkout_data}', array('as' => 'register-successful', function($checkout_data) {
        return view('public.register-successful')->with('checkout_data', $checkout_data);
    }));
Route::get('member-packages', ['as' => 'member-packages', 'uses' => 'IndexPageController@memberPackages'
]);

Route::group(['prefix' => 'payment'], function() {
    Route::get('checkout/{checkout}', ['as' => 'checkout', 'uses' => 'RegistrationController@checkout'
    ]);
});

//Routes for searching an artisan
//Route::get('artisans', ['as' => 'artisans', 'uses' => 'SearchArtisanController@index'
//]);
Route::get('search-results', ['as' => 'search.artisans', 'uses' => 'SearchArtisanController@search'
]);

//News detail route
Route::get('news/{id}/detail', ['as' => 'news.detail', 'uses' => 'IndexPageController@newsDetail']);

/*
  |----------------------------------------------------------------
  | Admin routes will be registered here
  |
 */

Route::group(['prefix' => 'aag-controlpanel'], function() {
    Route::get('', function() {
        return view('admin.dashboard');
    });
    //-------user routes------------
    Route::get('users', ['as' => 'users', 'uses' => 'UserController@index'
    ]);
    Route::get('user/{id}/edit', ['as' => 'user.edit', 'uses' => 'UserController@edit'
    ]);
    Route::get('user/{id}/select-staff', ['as' => 'user.selectstaff', 'uses' => 'UserController@selectStaff'
    ]);
    Route::put('user/{id}', ['as' => 'user.update', 'uses' => 'UserController@update'
    ]);
    Route::get('user/{id}', ['as' => 'user.destroy', 'uses' => 'UserController@destroy'
    ]);
    //--------end------------------
    
    //----------- staff routes-------
     Route::get('staff', ['as' => 'staff', 'uses' => 'StaffController@index'
    ]);
    Route::get('staff/{id}/edit', ['as' => 'staff.edit', 'uses' => 'StaffController@edit'
    ]);
    Route::get('staff/{id}', ['as' => 'staff.destroy', 'uses' => 'StaffController@destroy'
    ]);
    Route::put('staff/{id}', ['as' => 'staff.update', 'uses' => 'StaffController@update'
    ]);
    Route::post('staff', ['as' => 'staff.store', 'uses' => 'StaffController@store'
    ]);
    //----------end routes----------
    
    //-----------membership routes------------
    Route::get('members', ['as' => 'members', 'uses' => 'MemberController@index'
    ]);
    Route::get('members/{id}/remove-image', ['as' => 'member.removeImage', 'uses' => 'MemberController@removeMemberPicture'
    ]);
    Route::post('members', ['as' => 'member.save', 'uses' => 'MemberController@save'
    ]);
    Route::put('members/{id}', ['as' => 'member.update', 'uses' => 'MemberController@update'
    ]);
    Route::get('members/{id}/edit', ['as' => 'member.edit', 'uses' => 'MemberController@edit'
    ]);
    Route::get('members/filter-result', ['as' => 'member.filter', 'uses' => 'MemberController@filterMembers'
    ]);

    Route::get('members/{id}', ['as' => 'member.destroy', 'uses' => 'MemberController@destroy'
    ]);
    Route::get('artisan/detail/{id}', ['as' => 'artisan.detail', 'uses' => 'SearchArtisanController@memberDetail'
    ]);
    //-----------end-------------------------

    Route::get('packages', ['as' => 'packages', 'uses' => 'MemberLevelController@index'
    ]);
    Route::get('packages/{id}/edit', ['as' => 'packages.edit', 'uses' => 'MemberLevelController@edit'
    ]);
    Route::get('packages/{id}', ['as' => 'packages.destroy', 'uses' => 'MemberLevelController@destroy'
    ]);
    Route::put('packages/{id}', ['as' => 'packages.update', 'uses' => 'MemberLevelController@update'
    ]);
    Route::post('packages', ['as' => 'packages.store', 'uses' => 'MemberLevelController@store'
    ]);
    //export route
    Route::post('members/export-to-excel', ['as' => 'member.export', 'uses' => 'MemberController@exportToExcel'
    ]);
    //--------subscription routes-----------

    Route::get('member-subscriptions', ['as' => 'member-subscriptions', 'uses' => 'MemberSubscriptionController@index'
    ]);
    Route::post('member-subscription-save', ['as' => 'member-subscription-save', 'uses' => 'MemberSubscriptionController@saveMemberSubscription'
    ]);
    //----------end-------------------------
    //
    //news article routes
    Route::get('news', ['as' => 'news', 'uses' => 'NewsController@index'
    ]);
    Route::get('news/{id}/edit', ['as' => 'news.edit', 'uses' => 'NewsController@edit'
    ]);
    Route::put('news/{id}', ['as' => 'news.update', 'uses' => 'NewsController@update'
    ]);
    Route::get('news/{id}', ['as' => 'news.destroy', 'uses' => 'NewsController@destroy'
    ]);
    Route::post('news', ['as' => 'news.store', 'uses' => 'NewsController@store'
    ]);

    Route::get('events', ['as' => 'events', 'uses' => 'NewsController@index'
    ]);
    Route::get('event-save', ['as' => 'event-new', 'uses' => 'NewsController@saveEvent'
    ]);

    //------------gallery routes----------------
    Route::get('portfolios', ['as' => 'portfolios', 'uses' => 'GalleryController@index']);
    Route::get('portfolio/{id}/edit', ['as' => 'portfolio.edit', 'uses' => 'GalleryController@edit']);
    Route::get('portfolio/{id}', ['as' => 'portfolio.delete', 'uses' => 'GalleryController@destroy']);
    Route::post('portfolios', ['as' => 'portfolio.store', 'uses' => 'GalleryController@store']);
    Route::put('portfolio/{id}', ['as' => 'portfolio.update', 'uses' => 'GalleryController@update']);
    Route::get('portfolio/photos/{id}', ['as' => 'portfolio.addPhotos', 'uses' => 'GalleryController@addPhotos']);
    Route::post('portfolio/upload', ['as' => 'portfolio.upload', 'uses' => 'GalleryController@upload']);

    //--------------advert routes-------------------------
    Route::get('adverts', ['as' => 'adverts', 'uses' => 'AdvertController@index']);
    Route::get('advert/{id}/edit', ['as' => 'advert.edit', 'uses' => 'AdvertController@edit']);
    Route::get('advert/{id}', ['as' => 'advert.delete', 'uses' => 'AdvertController@delete']);
    Route::post('advert', ['as' => 'advert.save', 'uses' => 'AdvertController@save']);
    Route::put('advert/{id}', ['as' => 'advert.update', 'uses' => 'AdvertController@update']);

    //artisan categories route
    Route::get('artisan-category', ['as' => 'artisan-category', 'uses' => 'ArtisanCategoryController@index'
    ]);
    Route::get('artisan-category/{id}', ['as' => 'artisan-category-edit', 'uses' => 'ArtisanCategoryController@editCategory'
    ]);
    Route::post('artisan-category/{id}', ['as' => 'artisan-category-save', 'uses' => 'ArtisanCategoryController@saveArtisanCategory'
    ]);
});



