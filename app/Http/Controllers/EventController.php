<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class EventController extends Controller {

    public function index() {
        return view('admin.news-event.event')
                ->with('event', $this->getInputs(null))
                ->with('eventList', $this->loadAllEvents());
    }

    public function loadAllEvents() {
        $events = Event::orderBy('event_date', 'ASC')->paginate(10);
        return $events;
    }

    public function getInputs($event) {
        if ($event == null) {
            $event = new Event();
        }
        $event->title = Input::get('title');
        $event->event_body = Input::get('eventBody');
        $event->event_date = Input::get('eventDate');

        return $event;
    }

    public function saveEvent($id) {
        if($id == null){
            $event = $this->getInputs(new Event());
        }else{
            $event = Event::find($id);
        }

        if ($event->save() != null) {
            flash()->success('Saving successful');
        } else {
            flash()->error('Saving failed');
        }
        return $this->index();
    }
    
    public function edit($id){
        $event = Event::find($id);
        if($event != null){
            return view('admin.news-event.event')->with('event', $event)
                    ->with('events', $this->loadAllEvents());
        }else{
            return $this->index();
        }
    }

}
