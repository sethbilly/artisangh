<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Helpers\CommonService;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller {

    //put your code here
    public function index(CommonService $commonService) {
        return view('admin.users.user-account')->with('user', $this->getInputs(null))
                        ->with('userList', $this->loadAllUsers($commonService))
                        ->with('staffList', $commonService->loadStaff(5));
    }

    public function loadAllUsers(CommonService $commonService) {
        $userList = $commonService->loadUsers(5);
        return $userList;
    }

    public function create() {
        return $this->index();
    }

    private function getInputs($user) {
        if ($user == null) {
            $user = new User();
        }
        $password = Input::get('password');
        
        $user->user_status = Input::get('user_status');
        $user->is_admin = true;
        $user->password = Hash::make($password);
        return $user;
    }

    public function selectStaff($id, CommonService $commonService) {
        $user = User::find($id);
        return view('admin.users.user-edit')->with('user', $user)
                        ->with('staffList', $commonService->loadStaff(5));
    }

    public function edit($id, CommonService $commonService) {
        $user = User::find($id);
        if ($user != null) {
            return view('admin.users.user-edit')
                            ->with('user', $user)
                            ->with('userList', $this->loadAllUsers($commonService))
                            ->with('staffList', $commonService->loadStaff(5));
        } else {
            flash()->error('No such record found');
            return \Illuminate\Support\Facades\Redirect::route('users');
        }
    }

    public function update($id) {
        $newUser = User::find($id);
        $user = $this->getInputs($newUser);
        //$route = \Illuminate\Support\Facades\URL::route('user');
        if ($user->save() != null) {
            flash()->success('User created successfully');
            //return \Illuminate\Support\Facades\URL::route('user');
            return Redirect::route('users');
        } else {
            return redirect()->to('user/' . $id . '/select-staff')->withInput($user);
        }
    }

    public function destroy($id) {
        
    }

}
