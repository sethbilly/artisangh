<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Helpers\CommonService;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Member;
use App\MemberWork;

class SearchArtisanController extends Controller {

    public function index(CommonService $commonService) {
        return view('public.search-artisan.search-artisan')
                        ->with('categories', $commonService->loadArtisanCategories())
                        ->with('regions', $commonService->loadRegions())
                        ->with('artisans', $commonService->searchArtisans(null, null, null, null))
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function search(CommonService $commonService, Request $request) {
        //search artisans based on category, region, city or town
        $category = Input::get('category');

        $region = Input::get('region');
        $city = Input::get('city');
        $town = Input::get('town');
        $artisans = $commonService->searchArtisans($category, $region, $city, $town);
        $request->flash();
        return view('public.search-artisan.search-artisan')
                        ->with('categories', $commonService->loadArtisanCategories())
                        ->with('regions', $commonService->loadRegions())
                        ->with('artisans', $artisans)
                        ->with('premium_members', $commonService->loadPremiumMembers())
                        ->with('category', $category)
                        ->with('region', $region)->with('city', $city)->with('town', $town);
    }

    public function memberDetail($id, CommonService $commonService) {
        $memberDetail = Member::find($id);
        $works = Member::find($id)->works();
        if ($memberDetail != null) {
            return view('public.search-artisan.search-detail')->with('member_detial', $memberDetail)
                            ->with('premium_members', $commonService->loadPremiumMembers()) 
                    -> with('regions', $commonService->loadRegions()) 
                    ->with('categories', $commonService->loadArtisanCategories())
                            ->with('works', $works);
        }
    }

}
