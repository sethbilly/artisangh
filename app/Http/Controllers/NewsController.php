<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;

class NewsController extends Controller {

    public function index() {
        return view('admin.news-event.news')
                        ->with('news', $this->getInputs(null))
                        ->with('newsList', $this->loadAllNews());
    }
    
    public function create(){
        return $this->index();
    }
    
    public function store(){
        $news = $this->getInputs(new News());
        if ($news->save() != null) {
            flash()->success('Saving successful');
        } else {
            flash()->error('Saving failed');
        }
        return \Illuminate\Support\Facades\Redirect::route('news');
    }
    
    public function destroy($id){
        $news = News::find($id);
        if ($news != null) {
            if ($news->delete() != null) {
                flash()->success('Deleted successfully');
                return \Illuminate\Support\Facades\Redirect::route('news');
            } else {
                flash()->error('Deletion failed');
                return \Illuminate\Support\Facades\Redirect::route('news');
            }
        }
    }
    
    public function edit($id){
        $news = News::find($id);
        if ($news != null) {
            return view('admin.news-event.news-edit')
                            ->with('news', $news)
                            ->with('newsList', $this->loadAllNews());
        } else {
            flash()->error('No such record found');
            return \Illuminate\Support\Facades\Redirect::route('news');
        }
    }
    
    public function update($id){
         $news = News::find($id);
         $news = $this->getInputs($news);
         //$route = \Illuminate\Support\Facades\URL::route('news');
         if($news->save() != null){
             flash()->success('News updated successfully');
             //return \Illuminate\Support\Facades\URL::route('news');
             return Redirect::route('news');
         }else{
             return redirect()->to('news/' .$id .'/edit')->withInput($news);
         }
    }

    public function loadAllNews() {
        $newsList = News::orderBy('news_date', 'ASC')->paginate(3);
        return $newsList;
    }

    public function getInputs($news) {
        if ($news == null) {
            $news = new News();
        }
        $news->title = Input::get('title');
        $body = strip_tags(Input::get('body'), '<p>');
        $news->body = $body;
        $news->news_date = Input::get('newsDate');
        $news->published = Input::get('published');
        $news->summary = str_limit($body, 60, '...');
        if (Input::hasFile('featureImage')) {
//            $image = Input::file('featureImage');
//            $filename = time() . '.' . $image->getClientOriginalExtension();
//            $path = public_path('News/' . $filename);
//            Image::make($image->getRealPath())->resize(30, 30)->save(public_path('News/thumbnail/' . $filename));
//            $news->feature_image = 'News/' . $filename;
//            $news->thumbnail = 'News/thumbnail/' . $filename;
            $fileExtension = Input::file('featureImage')->getClientOriginalExtension();
            Input::file('featureImage')->move(public_path() . '/News', time() . '.' . $fileExtension);
            $news->feature_image = '/News/' . time() . '.' . $fileExtension;
            Image::make(public_path($news->feature_image))->resize(100, 100)->save(public_path('News/thumbnail/' . time() . '.' . $fileExtension));
            $news->thumbnail = 'News/thumbnail/' .time() . '.' . $fileExtension;
        }

        return $news;
    }
}
