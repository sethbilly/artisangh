<?php

namespace App\Http\Controllers;

use App\Helpers\CommonService;
use App\Http\Controllers\Controller;
use App\News;
use App\PortfolioResource;

class IndexPageController extends Controller {

    public function home(CommonService $commonService) {
        return view('public.home')
                        ->with('lastest_news', $commonService->loadLastestNews())
                        ->with('premium_members', $commonService->loadPremiumMembers())
                                ->with('adverts', $commonService->loadAdverts())
                                ->with('events', $commonService->loadEvents());
    }

    //--------ABOUT US----------------
    public function contactUs(CommonService $commonService) {
        return view('public.about-us.contact-us')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function whoWeAre(CommonService $commonService) {
        return view('public.about-us.who-we-are')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function mission(CommonService $commonService) {
        return view('public.about-us.mission')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function vision(CommonService $commonService) {
        return view('public.about-us.vision')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function objectives(CommonService $commonService) {
        return view('public.about-us.objectives')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    //----------END--------------------
    //--------SERVICES-----------------
    public function certification(CommonService $commonService) {
        return view('public.services.certification')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function jobPlacement(CommonService $commonService) {
        return view('public.services.job-placement')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function training(CommonService $commonService) {
        return view('public.services.training')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function assistance(CommonService $commonService) {
        return view('public.services.assistance')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    //--------END----------------------
    //--------PARTNERS-----------------
    public function aabn(CommonService $commonService) {
        return view('public.partners.aabn')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function globalCommunities(CommonService $commonService) {
        return view('public.partners.global-communities')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function hfcBoafo(CommonService $commonService) {
        return view('public.partners.hfc-boafo')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function yesGh(CommonService $commonService) {
        return view('public.partners.yes-gh')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function ocig(CommonService $commonService) {
        return view('public.partners.ocig')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function cotvet(CommonService $commonService) {
        return view('public.partners.cotvet')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function nvti(CommonService $commonService) {
        return view('public.partners.nvti')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    //--------END----------------------
    public function faqs(CommonService $commonService) {
        return view('public.faqs')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function partnerUs(CommonService $commonService) {
        return view('public.partner-us')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function gallery(CommonService $commonService) {
        $albums = PortfolioResource::with('portfolio')->paginate(6);
        return view('public.media-center.gallery')
                        ->with('premium_members', $commonService->loadPremiumMembers())
                        ->with('portfolios', $albums);
    }

    public function newsAndEvents(CommonService $commonService) {
        $news = News::where('published', TRUE)->orderBy('news_date', 'DESC')->paginate(3);
        return view('public.media-center.news-events')
                        ->with('premium_members', $commonService->loadPremiumMembers())
                        ->with('news', $news);
    }

    public function yedie(CommonService $commonService) {
        return view('public.programs.yiedie')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function memberPackages(CommonService $commonService) {
        return view('public.member-packages')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function searchArtisans(CommonService $commonService) {
        return view('public.search-artisan')
                        ->with('premium_members', $commonService->loadPremiumMembers());
    }

    public function newsDetail($id, CommonService $commonService) {
        $news = News::find($id);
        if ($news != null) {
            return view('public.media-center.news-detail')->
                            with('news', $news)
                            ->with('premium_members', $commonService->loadPremiumMembers());
        } else {
            return $this->home($commonService);
        }
    }

}
