<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\MemberSubscription;
use App\Helpers\CommonService;

class MemberSubscriptionController extends Controller{
   
    public function index(CommonService $commonService){
        return view('admin.membership.member-subscription')
        ->with('subscription', $this->getInputs(new MemberSubscription()))
                ->with('subscriptions', $this->loadAllSubscriptions())
                ->with('members', $commonService->loadArtisanMembers());
    }
    
    public function getInputs($subscription){
        if($subscription == null){
            $subscription = new MemberSubscription();
        }
        
        $subscription->member_id = Input::get("memberId");
        $subscription->level_id = Input::get("levelId");
        $subscription->payment_expires = Input::get("paymentExpires");
        $subscription->payment_made = Input::get("paymentMade");
        
        return $subscription;
    }
    
    public function saveMemberSubscription(){
        $subscription = $this->getInputs(new MemberSubscription());
        
        if($subscription->save() != null){
            flash()->success('Subscription successful');
        }
        else{
            flash()->error('Member subscription failed');
        }
        return $this->index();
    }
    
    public function loadAllSubscriptions(){
        $subscriptions = MemberSubscription::paginate(10);
        return $subscriptions;
    }
}
