<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Member;
use App\Helpers\CommonService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MemberController extends Controller {

    public function index(CommonService $commonService) {
//        $level_member = Member::with(array('memberlevel' => function($query){
//            $query->addSelect(array('level_id','level_name'));
//        }))->get();
        return view('admin.membership.member')
                        ->with('member', $this->getInputs(null))
                        ->with('memberList', $this->loadAllMembers($commonService))
                        ->with('regions', $commonService->loadRegions())
                        ->with('categories', $commonService->loadArtisanCategories())
                        ->with('levels', $commonService->loadMemberLevels());
    }

    public function loadAllMembers(CommonService $commonService) {
        $members = $commonService->filterMembers(null, null, null, null, null, null, null, 10);
        return $members;
    }

    private function getInputs($member) {
        if ($member == null) {
            $member = new Member();
        }
        $dor = Input::get('newsDate');
        $member->member_id = Input::get('memberID');
        $member->name = Input::get('name');
        $member->email = Input::get('email');
        $member->telephone = Input::get('telephone');
        $member->telephone1 = Input::get('telephone1');
//        if($dor)
//        {
        $member->dor = $dor;

//        }
        $member->address = Input::get('address');
        $member->city = Input::get('city');
        $member->region = Input::get('region');
        $member->town = Input::get('town');
        $member->level = Input::get('level');
        $member->payment_made = false;
        $member->verified = Input::get('verified');
        $skills = Input::get('category');
//        $artisanSkills = implode($skills, ',');
        $member->skill_having = $skills;
        $fileName = trim(Input::get('name'));
        if (Input::hasFile('memberPicture')) {
            $fileExtension = Input::file('memberPicture')->getClientOriginalExtension();
            Input::file('memberPicture')->move(public_path() . '/Member', $fileName . '.' . $fileExtension);
            $member->member_picture = '/Member/' . $fileName . '.' . $fileExtension;
        }

        return $member;
    }

    public function save(CommonService $commonService) {
        $member = $this->getInputs(new Member());
        if ($member->save() != null) {
            flash()->success('Saving successful');
        } else {
            flash()->error('Saving failed');
        }
        return $this->index($commonService);
    }

    public function edit($id, CommonService $commonService) {
        $member = Member::find($id);
        if ($member != null) {

            return view('admin.membership.member-edit')
                            ->with('member', $member)
                            ->with('memberList', $this->loadAllMembers($commonService))
                            ->with('regions', $commonService->loadRegions())
                            ->with('categories', $commonService->loadArtisanCategories())
                            ->with('levels', $commonService->loadMemberLevels());
        } else {
            flash()->error('No such record');
            return $this->index($commonService);
        }
    }

    public function update($id) {
        $member = Member::find($id);
        $member = $this->getInputs($member);
        $route = \Illuminate\Support\Facades\URL::route('members');
        if ($member->save() != null) {
            flash()->success('Member updated successfully');
            return \Illuminate\Support\Facades\Redirect::route('members');
        } else {
            return redirect()->to('members/' . $id . '/edit')->withInput($member);
        }
    }

    public function destroy($id) {
        $member = Member::find($id);
        if ($member != null) {
            if ($member->delete() != null) {
                flash()->success('Deleted successfully');
                return \Illuminate\Support\Facades\Redirect::route('members');
            } else {
                flash()->error('Deletion failed');
                return \Illuminate\Support\Facades\Redirect::route('members');
            }
        }
    }

    public function removeMemberPicture($id) {
        $member = Member::find($id);
        if ($member != null) {
            //find file path of the member image
//            $filename = Input::get();
            //$filepath = public_path().$member->member_picture;
            if (Storage::delete(public_path() . $member->member_picture)) {
                flash()->success("Image removed succesfully");
                return \Illuminate\Support\Facades\Redirect::route('members');
            } else {
                flash()->error("Failed to remove image. Please try again");
                return \Illuminate\Support\Facades\Redirect::route('members');
            }
        }
    }

    public function filterMembers(CommonService $commonService, Request $request) {
        $name = Input::get('searchName');
        $category = Input::get('searchCategory');
        $region = Input::get('searchRegion');
        $city = Input::get('searchCity');
        $town = Input::get('searchTown');
        $level = Input::get('searchLevel');
        $has_paid = Input::get('paid');
        $rowSize = 10;

        $members = $commonService->filterMembers($name, $category, $level, $has_paid, $region, $town, $city, $rowSize);
        $request->flash();
        return view('admin.membership.member')
                        ->with('member', $this->getInputs(null))
                        ->with('memberList', $members)
                        ->with('regions', $commonService->loadRegions())
                        ->with('categories', $commonService->loadArtisanCategories())
                        ->with('levels', $commonService->loadMemberLevels());
    }

    public function exportToExcel() {
//        $excel = App::make('excel');
//        Excel::create('Members', function($excel) {
//
//            $excel->sheet('Members', function($sheet) {
//                $users = Member::orderBy('created_at', 'desc')->get();
//                $sheet->fromArray($users);
//            });
//        })->download('xls');
        Excel::create('Filename', function($excel) {
            $excel->sheet('Excel sheet', function($sheet) {
                
            });
        })->download('xls');
    }

}
