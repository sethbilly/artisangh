<?php
namespace App\Http\Controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StaffController
 *
 * @author Billy
 */
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class StaffController extends Controller {

    //put your code here
    public function index() {
        return view('admin.users.staff')
                        ->with('staff', $this->getInputs(null))
                        ->with('staffList', $this->loadAllStaff());
    }

    public function store() {
        $staff = $this->getInputs(new User());
        if ($staff->save() != null) {
            flash()->success('Saving successful');
        } else {
            flash()->error('Saving failed');
        }
        return \Illuminate\Support\Facades\Redirect::route('staff');
    }

    public function create() {
        return $this->index();
    }

    public function edit($id) {
        $staff = User::find($id);
        if ($staff != null) {
            return view('admin.users.staff-edit')
                            ->with('staff', $staff)
                            ->with('staffList', $this->loadAllStaff());
        } else {
            flash()->error('No such record found');
            return \Illuminate\Support\Facades\Redirect::route('staff');
        }
    }

    public function update($id) {
        $staff = User::find($id);
         $staff = $this->getInputs($staff);
         //$route = \Illuminate\Support\Facades\URL::route('staff');
         if($staff->save() != null){
             flash()->success('User updated successfully');
             //return \Illuminate\Support\Facades\URL::route('staff');
             return Redirect::route('staff');
         }else{
             return redirect()->to('staff/' .$id .'/edit')->withInput($staff);
         }
    }

    public function destroy($id) {
         $staff = User::find($id);
        if ($staff != null) {
            $staff->deleted = true;
            if ($staff->save() != null) {
                flash()->success('Deleted successfully');
                return \Illuminate\Support\Facades\Redirect::route('staff');
            } else {
                flash()->error('Deletion failed');
                return \Illuminate\Support\Facades\Redirect::route('staff');
            }
        }
    }

    private function getInputs($staff) {
        if ($staff == null) {
            $staff = new User();
        }
        $staff->name = Input::get('name');
        $staff->email = Input::get('email');
        $staff->telephone = Input::get('telephone');
        $staff->telephone1 = Input::get('telephone1');
        $staff->address = Input::get('address');
        if(Input::get('staff_status') === 1){
            $staff->staff_status = true;
        }else{
            $staff->staff_status = false;
        }
        return $staff;
    }

    public function loadAllStaff() {
        $staffList = User::where('deleted', false)->paginate(10);
        return $staffList;
    }

}
