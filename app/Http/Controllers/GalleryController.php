<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Portfolio;
use App\PortfolioResource;

/**
 * Description of GalleryController
 *
 * @author seth
 */
class GalleryController extends Controller {

    //put your code here
    public function index() {
        return view('admin.gallery.gallery')
                        ->with('portfolioList', $this->loadAllPortfolios())
                        ->with('gallery', $this->getInputs(null));
    }

    public function store() {
        $portfolio = $this->getInputs(new Portfolio());
        if ($portfolio->save() != null) {
            flash()->success('Saved Successfully. You can add photos now');
        } else {
            return redirect()->to('portfolios')->withInput($portfolio);
        }
    }

    private function getInputs($gallery) {
        if ($gallery == null) {
            $gallery = new Portfolio();
        }
        $gallery->title = Input::get('title');
        $gallery->desc = Input::get('desc');
        return $gallery;
    }

    private function loadAllPortfolios() {
        $portfolioList = Portfolio::orderBy('created_at', 'DESC')->paginate(5);
        return $portfolioList;
    }

    public function edit($id) {
        $portfolio = Portfolio::find($id);
        if ($portfolio != null) {
            return view('admin.gallery.gallery-edit')
                            ->with('gallery', $portfolio)
                            ->with('portfolioList', $this->loadAllPortfolios());
        } else {
            flash()->error('No such record found');
            return \Illuminate\Support\Facades\Redirect::route('portfolios');
        }
    }

    public function update($id) {
        
    }

    public function destroy($id) {
        $portfolio = Portfolio::find($id);
        if ($portfolio != null) {
            if ($portfolio->delete() != null) {
                flash()->success('Deleted successfully');
                return \Illuminate\Support\Facades\Redirect::route('portfolios');
            } else {
                flash()->error('Deletion failed');
                return \Illuminate\Support\Facades\Redirect::route('portfolios');
            }
        }
    }

    //portfolio_resources functions
    public function upload($id) {
        $portfolio = Portfolio::find($id);
        $image = Input::file('file');

        foreach ($image as $image)
        {
            $fileName = $image->getClientOriginalName();
            $image->move(public_path() . '/Gallery', $fileName);
            $resource_url = '/Gallery/' . $fileName;

            $portfolio_resource = new PortfolioResource(
                    ['resource_url' => $resource_url]);
            $portfolio->portfolioImages()->save($portfolio_resource);
            
        }
        
    }

    public function addPhotos($id) {
        return view('admin.gallery.album')
                        ->with('id', $id);
    }

}
