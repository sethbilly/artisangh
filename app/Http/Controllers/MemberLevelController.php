<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\MemberLevel;
use Illuminate\Support\Facades\Redirect;

class MemberLevelController extends Controller {

    public function index() {
         return view('admin.membership.member-level')
                ->with('memberLevel', $this->getInputs(null))
                ->with('memberLevelList', $this->loadAllMemberLevels());
    }

    public function create(){
        return $this->index();
    }

    public function store(){
        $memberLevel = $this->getInputs(new MemberLevel());

        if ($memberLevel->save() != null) {
            flash()->success('Saving successful');
        } else {
            flash()->error('Saving failed');
        }
        return Redirect::route('packages');
    }

    public function getInputs($memberLevel) {
        if ($memberLevel == null) {
            $memberLevel = new MemberLevel();
        }
        $memberLevel->level_name = Input::get('levelName');
        $memberLevel->level_price = Input::get('levelPrice');
        $memberLevel->level_period = Input::get('levelPeriod');
        if(Input::get('isActive') === 1){
            $memberLevel->is_active = true;
        }else{
            $memberLevel->is_active = true;
        }
        $memberLevel->description = Input::get('description');
        
        return $memberLevel;
    }

    public function edit($id){
        $memberLevel = MemberLevel::find($id);
        if($memberLevel != null){
            return view('admin.membership.member-level-edit')
                ->with('memberLevel', $memberLevel)
                ->with('memberLevelList', $this->loadAllMemberLevels());
        }else{
            flash()->error("No such record found");
        }
    }

    public function update($id){
        $memberLevel = MemberLevel::find($id);
        if($memberLevel != null){
            $memberLevel = $this->getInputs($memberLevel);
            if($memberLevel->save() != null){
                flash()->success('Updated successfully');
                return Redirect::route('packages');
            }else{
                return Redirect::route('package/'. $id .'/edit')->withInputs($memberLevel);
            }
        }
    }

    public function destroy($id){
        $memberLevel = MemberLevel::find($id);
        if($memberLevel != null){
            if($memberLevel->delete() != null){
                flash()->success('Deleted successfully');
                return Redirect::route('packages');
            }else{
                flash()->success('Deletion of level failed');
                return Redirect::route('packages');
            }
            
        }

    }


    public function loadAllMemberLevels() {
        $memberLevels = MemberLevel::orderBy('level_name', 'ASC')->paginate(5);
        return $memberLevels;
    }

}
