<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Adverts;
use Illuminate\Support\Facades\Input;
/**
 * Description of AdvertController
 *
 * @author seth
 */
class AdvertController extends Controller{
    //put your code here
    public function index(){
        return view('admin.advert.advert')
                        ->with('advert', $this->getInputs(null))
                        ->with('advertList', $this->loadAdverts());
    }
    
    public function save(){
        $advert = $this->getInputs(new Adverts());
        if ($advert->save() != null) {
            flash()->success('Saving successful');
        } else {
            flash()->error('Saving failed');
        }
        return \Illuminate\Support\Facades\Redirect::route('adverts');
    }
    
    private function getInputs($advert){
        if ($advert == null) {
            $advert = new Adverts();
        }
        $advert->title = Input::get('title');
        $advert->advert_date = Input::get('advertDate');
        $advert->published = Input::get('published');
        $advert->description = Input::get('desc');
        if (Input::hasFile('featureImage')) {
            $fileExtension = Input::file('featureImage')->getClientOriginalExtension();
            Input::file('featureImage')->move(public_path() . '/Adverts', time() . '.' . $fileExtension);
            $advert->feature_image = '/Adverts/' . time() . '.' . $fileExtension;
        }

        return $advert;
    }
            
    
    public function edit($id){
        $advert = Adverts::find($id);
        if($advert != null){
            return view('admin.advert.advert-edit')->with('advert', $advert)
                ->with('advertList', $this->loadAdverts());
        }else{
            flash()->error('No such record found!');
            return \Illuminate\Support\Facades\Redirect::route('adverts');
        }
    }
    
    public function update($id){
         $advert = Adverts::find($id);
         $advert = $this->getInputs($advert);
         //$route = \Illuminate\Support\Facades\URL::route('news');
         if($advert->save() != null){
             flash()->success('Advert updated successfully');
             //return \Illuminate\Support\Facades\URL::route('news');
             return Redirect::route('adverts');
         }else{
             return redirect()->to('advert/' .$id .'/edit')->withInput($advert);
         }
    }
    
    public function delete($id){
        $advert = Adverts::find($id);
        if ($advert != null) {
            if ($advert->delete() != null) {
                flash()->success('Deleted successfully');
                return \Illuminate\Support\Facades\Redirect::route('adverts');
            } else {
                flash()->error('Deletion failed');
                return \Illuminate\Support\Facades\Redirect::route('adverts');
            }
        }
    }
    
    public function loadAdverts(){
        $adverts = Adverts::orderBy('created_at', 'DESC')->paginate(10);
        return $adverts;
    }
}
