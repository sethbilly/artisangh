<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Helpers\CommonService;
use App\Member;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\MemberSubscription;
use App\Http\Requests\Request;
use App\MemberLevel;

/**
 * Description of RegistrationController
 *
 * @author seth
 */
class RegistrationController extends Controller {

    public function register(CommonService $commonService) {
        return view('public.register')
                        ->with('categories', $commonService->getArtisanCategories())
                        ->with('regions', $commonService->loadRegions())
                        ->with('levels', $commonService->loadMemberLevels())
                        ->with('art_cats', $commonService->loadArtisanCategories());
    }

    private function getInputs($member) {
        $artisanSkills = implode(",", Input::get('categoryName'));
        $member->member_id = Input::get('memberId');
        $member->name = Input::get('name');
        $member->email = Input::get('email');
        $member->telephone = Input::get('telephone');
        $member->telephone1 = Input::get('telephone1');
        $member->address = Input::get('address');
        $member->city = Input::get('city');
        $member->region = Input::get('region');
        $member->town = Input::get('town');
        $member->skill_having = $artisanSkills;
        $member->level = Input::get('level');
        $member->payment_made = false;

        if (Input::hasFile('memberPicture')) {
            $fileExtension = Input::file('memberPicture')->getClientOriginalExtension();
            Input::file('memberPicture')->move(public_path() . '/Member', $member->name . '.' . $fileExtension);
            $member->member_picture = '/Member/' . $member->name . '.' . $fileExtension;
        }

        return $member;
    }

    private function getClientInputs($client) {
        $skillsString = implode(",", Input::get('categoryName'));
        $client->name = Input::get('name');
        $client->email = Input::get('email');
        $client->telephone = Input::get('telephone');
        $client->telephone1 = Input::get('telephone1');
        $client->address = Input::get('address');
        $client->city = Input::get('city');
        $client->region = Input::get('region');
        $client->town = Input::get('town');
        $client->skill_needed = $skillsString;

//        if (Input::hasFile('clientPicture')) {
//            $fileExtension = Input::file('clientPicture')->getClientOriginalExtension();
//            Input::file('clientPicture')->move(public_path() . '/Client', $client->name . '.' . $fileExtension);
//            $client->client_picture = '/Client/' . $client->name . '.' . $fileExtension;
//        }

        return $client;
    }

    public function registerMember() {
        $accoutType = Input::get('client');
        if ($accoutType == 'artisan') {
            $member = $this->getInputs(new Member());
            if ($member->save() != null) {
                flash()->success('Registration successful');
                return Redirect::route('register-successful', array('checkout_data' => $member));
            } else {
                flash()->error('Registration unsuccessful');
                return back()->withInput();
            }
        } else {
            $client = $this->getClientInputs(new \App\Client());
            if ($client->save() != null) {
                flash()->success('Registration successful');
                return redirect()->route('register-successful');
            } else {
                flash()->error('Registration unsuccessful');
                return back()->withInput();
            }
        }
    }
    
    public function checkout($checkout){
        
        $member = Member::find($checkout);
        if($member !=null){
            $level_id = $member->level;
            //find level amount and description
            $level = MemberLevel::find($level_id);
            $order_id = 'ord-'.rand();
            $trans_id = rand();
            return view('public.checkout')->with('member_details', $member)
                    ->with('order_id', $order_id)
                    ->with('trans_id', $trans_id)
                    ->with('level', $level);
            
        }
    }

}
