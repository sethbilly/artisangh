<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\ArtisanCategory;
use Illuminate\Support\Facades\Input;

class ArtisanCategoryController extends Controller {

    public function index() {
        return view('admin.artisan-category.artisan-category')
                        ->with('artisanCategory', $this->getInputs(null))
                        ->with('artisanCatList', $this->loadArtisanCategories());
    }

    public function getInputs($artisanCat) {
        if ($artisanCat == null) {
            $artisanCat = new ArtisanCategory();
        }
        $artisanCat->category_name = Input::get('categoryName');
        $artisanCat->description = Input::get('description');

        return $artisanCat;
    }

    public function saveArtisanCategory($id) {
        if ($id == null) {
            $artisanCat = $this->getInputs(new ArtisanCategory());
        } else {
            $artisanCat = ArtisanCategory::find($id);
            $artisanCat = $this->getInputs($artisanCat);
        }

        if ($artisanCat->save() != null) {
            flash()->success('Saving successful');
        } else {
            flash()->error('Saving failed');
        }
        return $this->index();
    }
    
    public function editCategory($id) {
        $artisanCategory = ArtisanCategory::find($id);
        if ($artisanCategory != null) {
            return view('admin.artisan-category.artisan-category')
                            ->with('artisanCategory',$artisanCategory)
                            ->with('artisanCatList', $this->loadArtisanCategories());
        } else {
            flash()->error('An error occured in fetching data');
            return $this->index();
        }
    }

    public function loadArtisanCategories() {
        $artisanCats = ArtisanCategory::orderBy('category_name', 'ASC')->paginate(10);
        return $artisanCats;
    }

}
