<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberSubscription extends Model {

    protected $table = "member_subscription";

    public function member() {
       return $this->hasOne('App\Member');
    }

    public function level() {
        return $this->hasOne('App\MemberLevel');
    }

}
