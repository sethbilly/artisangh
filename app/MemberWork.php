<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MemberWork
 *
 * @author seth
 */
namespace App;
use Illuminate\Database\Eloquent\Model;

class MemberWork extends Model{
    //put your code here
    protected $table = 'member_work';
    
    public function member(){
        return $this->belongsTo('App\Member');
    }
}
