<?php

namespace App\Helpers;

use App\ArtisanCategory;
use App\Member;
use App\MemberLevel;
use App\News;
use App\Adverts;
use App\User;

class CommonService implements CommonServiceInterface {

    public function loadArtisanCategories() {
        $categories = ArtisanCategory::lists('category_name', 'id');
        return $categories->toArray();
    }

    public function getArtisanCategories() {
        $categories = ArtisanCategory::all();
        return $categories;
    }

    public function loadMemberLevels() {
        $memberLevels = MemberLevel::lists('level_name', 'id')->all();
        return $memberLevels;
    }

    public function loadRegions() {
        $regions = collect(['Ashanti', 'Brong-Ahafo', 'Central', 'Eastern', 'Greater Accra',
            'Northern', 'Upper East', 'Upper West', 'Volta', 'Western'])->all();
        return $regions;
    }

    //function will load all members who are artisans
    public function loadArtisanMembers() {
        $members = Member::where('is_client', 0);
        return $members;
    }

    public function loadLastestNews() {
        $news = News::where('published', TRUE)->orderBy('news_date', 'DESC')->paginate(2);
        return $news;
    }

    public function searchArtisans($category, $region, $town, $city) {
        $artisans = Member::where('skill_having', 'LIKE', '%' . $category . '%')
                        ->where('region', 'LIKE', '%' . $region . '%')
                        ->where('town', 'LIKE', '%' . $town . '%')
                        ->where('city', 'LIKE', '%' . $city . '%')->paginate(6);
        return $artisans;
    }

    public function searchArtisansLimit($category, $region, $town, $city, $rowSize) {
        $artisans = Member::where('skill_having', 'LIKE', '%' . $category . '%')
                        ->where('region', 'LIKE', '%' . $region . '%')
                        ->where('town', 'LIKE', '%' . $town . '%')
                        ->where('city', 'LIKE', '%' . $city . '%')->paginate($rowSize);
        return $artisans;
    }

    public function filterMembers($name, $category, $level, $has_paid, $region, $town, $city, $rowSize) {
        $members = Member::where('skill_having', 'LIKE', '%' . $category . '%')
                        ->where('level', 'LIKE', '%' . $level . '%')
                        ->where('payment_made', 'LIKE', '%' . $has_paid . '%')
                        ->where('region', 'LIKE', '%' . $region . '%')
                        ->where('town', 'LIKE', '%' . $town . '%')
                        ->where('name', 'LIKE', '%'.$name.'%')
                        ->where('city', 'LIKE', '%' . $city . '%')->orderBy('created_at', 'DESC')->paginate($rowSize);
//        $members = Member::with('level')->get();
        return $members;
    }

    public function loadPremiumMembers() {
        $premiumMembers = Member::where('level', '=', 3)->orderBy('created_at', 'ASC')->simplePaginate(8);
        return $premiumMembers;
    }
    
    public function loadPortfolios() {
        
    }
    
    public function loadAdverts() {
        $adverts = Adverts::where('published', '=', 1)->orderBy('advert_date', 'DESC')->get();
        return $adverts;
    }
    
    public function loadEvents() {
        
    }

    public function loadLevelWithMembers() {
        $members = Member::all();
        return $members;
    }
    
    public function loadStaff($rowSize) {
        $staffList = User::where('staff_status', '=', 1)
                ->where('user_status', '=', '0')->simplePaginate($rowSize);
        return $staffList;
    }
    
    public function loadUsers($rowSize) {
        $userList = User::where('is_admin', '=', 1)->simplePaginate($rowSize);
        return $userList;
    }
}
