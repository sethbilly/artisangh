<?php

namespace  App\Helpers;

interface CommonServiceInterface {
    
    public function loadArtisanCategories();
    
    public function loadMemberLevels();
    
    public function loadRegions();
    
    public function loadLastestNews();
    
    public function searchArtisans($category, $region, $town, $city);
    
    public function filterMembers($name, $category, $level, $has_paid, $region, $town, $city, $rowSize);
    
    public function loadPremiumMembers();
    
    public function loadPortfolios();
    
    public function loadAdverts();
    
    public function loadEvents();
    
    public function loadLevelWithMembers();
    
    public function loadStaff($rowSize);
    
    public function loadUsers($rowSize);
}
