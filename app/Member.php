<?php


namespace App;
use Illuminate\Database\Eloquent\Model; 
//use Sofa\Eloquence\Eloquence;

class Member extends Model{
    //use Eloquence;
    protected  $table = 'member';
   
    public function memberlevel(){
        return $this->belongsTo('App\MemberLevel', 'level','id');
    }
    
    public function skills(){
        return $this->belongsTo('App\ArtisanCategory', 'skill_having', 'id');
    }
    
    
    public function works(){
        return $this->hasMany('App\MemberWork');
    }
    
   
    
}
