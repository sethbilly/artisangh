<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('member', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('telephone');
            $table->string('telephone1')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('town')->nullable();
            $table->string('region')->change();
            $table->string('member_id')->nullable();
            $table->string('member_picture')->nullable();
            $table->boolean('is_client');
            $table->timestamps();
        });
        
        Schema::table('member', function ($table) {
            $table->dropColumn('region');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('membership');
    }

}
