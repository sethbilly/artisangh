searchVisible = 0;
transparent = true;

$(document).ready(function () {
    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();

    $('.wizard-card').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',
        onInit: function (tab, navigation, index) {

            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;
            $width = 100 / $total;

            $display_width = $(document).width();

            if ($display_width < 600 && $total > 3) {
                $width = 50;
            }

            navigation.find('li').css('width', $width + '%');

        },
        onNext: function (tab, navigation, index) {
            if (index == 1) {
                return validateFirstStep();
            } else if (index == 2) {
                return validateSecondStep();
            } else if (index == 3) {
                return validateThirdStep();
            } else if (index == 4) {
                return validateFourthStep();
            }

        },
        onTabClick: function (tab, navigation, index) {
            // Disable the posibility to click on tabs
            return false;
        },
        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;

            var wizard = navigation.closest('.wizard-card');

            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $(wizard).find('.btn-next').hide();
                $(wizard).find('.btn-finish').show();
            } else {
                $(wizard).find('.btn-next').show();
                $(wizard).find('.btn-finish').hide();
            }
        }
    });

    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {
        readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function () {
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked', 'true');
    });

//    $('.client').click(function () {
//        if ($(this).hasClass('active')) {
//            //$(this).removeClass('active');
//            $(this).find('.client').removeAttr('checked');
//        } else {
//            $(this).addClass('active');
//            $(this).find('.client').attr('checked', 'true');
//        }
//    });
    var artisan;
    $("input[name='client']").change(function () {
        $("input[name='client']").not(this).prop('checked', false);
        artisan = $("input[name='client']:checked").val();
        if (artisan === 'client') {
            $('.member_pack').hide();
            $('.picture-container').hide();
        } else {
            $('.member_pack').show();
            $('.picture-container').show();
        }

    });

    $height = $(document).height();
    $('.set-full-height').css('height', $height);

    //limit the number of checkboxes an artisan or client can select
    var maxAllowed;
    $("input[name='categoryName[]']").change(function () {
        var cnt = $("input[name='categoryName[]']:checked").length;
        if (artisan === 'artisan') {
            maxAllowed = 3;
        }
        if (cnt > maxAllowed) {
            $(this).prop("checked", "");
            alert('You can select maximum ' + maxAllowed + ' categories!!');
        }
    });

});

function validateFirstStep() {

    $(".wizard-card form").validate({
        rules: {
            client: {
                required: true,
                maxlength: 1
            }
        },
        messages: {
            client: "You must select at least one account type"
        }
    });

    if (!$(".wizard-card form").valid()) {
        console.log('invalid');
        return false;
    }
//    return true;

}

function validateSecondStep() {
    $(".wizard-card form").validate({
        rules: {
            name: "required",
            telephone: {
                required: true,
                digits: true
            },
            telephone1: {
                digits: true
            }
        },
        messages: {
            name: "Please enter your full name",
            telephone: "Please enter a valid phone number",
            telephone1: "Please enter a valid phone number"
        }
    });

    if (!$(".wizard-card form").valid()) {
        //form is invalid
        return false;
    }

//    return true;
}



function validateThirdStep() {
    //code here for third step
    $(".wizard-card form").validate({
        rules: {
        },
        messages: {
        }
    });
}

function validateFourthStep() {
    $(".wizard-card form").validate({
        rules: {
        },
        messages: {
        }
    });
}

//Function to show image before upload

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}















