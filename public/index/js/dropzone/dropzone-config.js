//var photo_counter = 0;
//Dropzone.options.myAwesomeDropZone = {
//
//    uploadMultiple: false,
//    parallelUploads: 100,
//    maxFilesize: 8,
//    previewsContainer: '#dropzonePreview',
//    previewTemplate: document.querySelector('#preview-template').innerHTML,
//    addRemoveLinks: true,
//    dictRemoveFile: 'Remove',
//    dictFileTooBig: 'Image is bigger than 8MB',
//
//    // The setting up of the dropzone
//    init:function() {
//
//        this.on("removedfile", function(file) {
//
//            $.ajax({
//                type: 'POST',
//                url: 'upload/delete',
//                data: {id: file.name},
//                dataType: 'html',
//                success: function(data){
//                    var rep = JSON.parse(data);
//                    if(rep.code == 200)
//                    {
//                        photo_counter--;
//                        $("#photoCounter").text( "(" + photo_counter + ")");
//                    }
//
//                }
//            });
//
//        } );
//    },
//    error: function(file, response) {
//        if($.type(response) === "string")
//            var message = response; //dropzone sends it's own error messages in string
//        else
//            var message = response.message;
//        file.previewElement.classList.add("dz-error");
//        _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
//        _results = [];
//        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
//            node = _ref[_i];
//            _results.push(node.textContent = message);
//        }
//        return _results;
//    },
//    success: function(file,done) {
//        photo_counter++;
//        $("#photoCounter").text( "(" + photo_counter + ")");
//    }
//}
//
//
//
//var baseUrl = "{{ url('/') }}";
//var token = "{{ Session::getToken() }}";
//Dropzone.autoDiscover = false;
//var myDropzone = new Dropzone("div#dropzoneFileUpload", {
//    url: baseUrl + "/portfolios",
//    params: {
//        _token: token
//    }
//});
Dropzone.options.realDropZone = {
    paramName: 'file',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    autoProcessQueue: true,
    previewsContainer: ".dropzone",
    uploadMultiple: false,
    parallelUploads: 25,
    maxFiles: 25,
//    init: function () {
//        var submitButton = document.querySelector("#submit-all")
//        realDropZone = this; // closure
//
//        submitButton.addEventListener("click", function (e) {
//            e.preventDefault();
//            e.stopPropagation();
//            realDropZone.processQueue(); // Tell Dropzone to process all queued files.
//        });
//
//// You might want to show the submit button only when 
//// files are dropped here:
//        this.on("addedfile", function () {
//            // Show submit button here and/or inform user to click it.
//        });
//        this.on("complete", function (file) {
//            realDropZone.removeFile(file);
//        });
//    }

};