@section('title', 'Login')
@include('partials.admin-header')
<body class="page-login-v3 layout-full">
    <!-- Page -->
    <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
         data-animsition-out="fade-out">>
        <div class="page-content vertical-align-middle">
            <div class="panel">
                <div class="panel-body">
                    <div class="brand">
                        <h2 class="brand-text font-size-18"></h2>
                    </div>
                    <form method="post" action="#">
                        <div class="form-group form-material floating">
                            <input type="username" class="form-control" name="username" />
                            <label class="floating-label">Username</label>
                        </div>
                        <div class="form-group form-material floating">
                            <input type="password" class="form-control" name="password" />
                            <label class="floating-label">Password</label>
                        </div>
                        <div class="form-group clearfix">
                            <a class="pull-right" href="forgot-password.html">Forgot password?</a>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-lg margin-top-40">Sign in</button>
                    </form>
                    <p>Still no account? Please go to <a href="javascript:void(0)">Register</a></p>
                </div>
            </div>

        </div>
    </div>
</body>
@include('partials.admin-footer')
</html>