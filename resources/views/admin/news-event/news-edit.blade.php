@extends('layouts.admin')

@section('date-picker')
<!--<link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css'>-->
@stop
@section('tinymce')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>


<script>tinymce.init({selector: 'textarea'});</script>

@endsection

@section('page-title', 'News')

@section('panel-title', 'Recent News')

@section('content')
<div class="col-sm-12">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">News Form</h4>
        <div class="example">
            {!! Form::model($news, array('method'=>'put', 'route'=>array('news.update', $news->id), 'files'=>true)) !!}
            @include('partials.inputs.news-input')
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- End Example Basic Form -->
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12">
        <!-- Example Basic Form Without Label -->
        <div class="example-wrap">
            <h4 class="example-title">Recent News</h4>
            <div class="example">
                <!-- category table comes here -->
                <table class="table-responsive table-bordered table-striped">
                    <tr>
                        <th>Title </th>
                        <th>Publication Date</th>
                        <th>Published</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    @forelse($newsList as $news)
                    <tr>
                        <td>{{$news->title}}</td>
                        <td>{{$news->news_date}}</td>
                        <td>{{$news->published}}</td>
                        <td>
                            <a href="{{URL::route('news.edit', $news->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                                <span class="icon wb-trash"></span>
                            </a>
                        </td>
                        <td>
                            <a href="{{URL::route('news.destroy', $news->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                                <span class="icon wb-trash"></span>
                            </a>
                        </td>
                    </tr>
                    @empty
                    <p>No news available</p>
                    @endforelse
                </table>
            </div>
            {!! $newsList->render() !!}
        </div>
        <!-- End Example Basic Form Without Label -->
    </div>
    @endsection()