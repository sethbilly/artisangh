@extends('layouts.admin')

@section('page-title', 'Users')

@section('panel-title', 'Users')

@section('content')
<div class="col-sm-12">
    <div class="col-sm-6">
        <div class="example-wrap">
            <h5 class="example-title"></h5>
            <div class="example">
                <table class="table-responsive table-bordered table-striped">
                    <tr>
                        <th>Name </th>
                        <th>Telephone</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    @forelse($staffList as $staff)
                    <tr>
                        <td>{{$staff->name}}</td>
                        <td>{{$staff->telephone}}</td>
                        <td>{{$staff->emal}}</td>
                        <td>
                            <a href="{{URL::route('user.selectstaff', $staff->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Add To Users">
                                <span class="icon wb-edit"></span>
                            </a>
                        </td>
                    </tr>
                    @empty
                    <p>No staff available</p>
                    @endforelse
                </table>
                {!! $staffList->render() !!}
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="example-wrap">
            <h4 class="example-title">User Account Form</h4>
            <div class="example">
                {!! Form::model($user, array('method'=>'put','route'=>array('user.update', $user->id))) !!}
                @include('partials.inputs.user-account-input')
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-info">Clear</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- End Example Basic Form -->
        </div>
    </div>
    <!-- Example Basic Form -->

</div>
<div class="clearfix"></div>
@include('partials.lists.user-account-list')
@endsection()
