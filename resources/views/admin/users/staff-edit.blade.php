@extends('layouts.admin')

@section('page-title', 'Staff')

@section('panel-title', 'Staff')

@section('content')
<div class="col-sm-12">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Staff Form</h4>
        <div class="example">
            {!! Form::model($staff, array('method'=>'put', 'route'=>array('staff.update', $staff->id))) !!}
            @include('partials.inputs.staff-input')
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="{{URL::route('staff')}}" class="btn btn-primary">Clear</a>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- End Example Basic Form -->
    </div>
</div>
@include('partials.lists.staff-list')
@endsection()