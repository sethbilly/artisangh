
@extends('layouts.admin')

@section('page-title', 'Porfolio')

@section('panel-title', 'Manage Portfolio')

@section('content')
<div class="col-sm-12">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Portfolio Form</h4>
        <div class="example">
            {!! Form::model($gallery, array('method'=>'put', 'route'=>array('portfolio.update', $gallery->id))) !!}
            @include('partials.inputs.gallery-input')
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                 <a href="{{URL::route('portfolios')}}" class="btn btn-primary">Clear</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Example Basic Form -->
</div>
@include('partials.lists.portfolio-list')
@endsection()