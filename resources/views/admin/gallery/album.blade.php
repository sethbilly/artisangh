@extends('layouts.admin')

@section('page-title', 'Gallery')

@section('panel-title', 'Manage Gallery')

@section('content')
<div class="col-sm-12">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Gallery Form</h4>
        <div class="example">
            <div id="dropzone">
                {!! Form::open(array('route'=>'portfolio.upload', 'files'=>true, 'class' => 'dropzone', 'id'=>'real-dropzone')) !!}
                    @include('partials.inputs.album-input')
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <!-- End Example Basic Form -->
</div>
@endsection()