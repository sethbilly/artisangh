@extends('layouts.admin')

@section('page-title', 'Gallery')

@section('panel-title', 'Manage Gallery')

@section('content')
<div class="col-sm-12">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Gallery Form</h4>
        <div class="example">
            <div id="dropzone">
                {!! Form::open(array('route'=>'portfolio.store')) !!}
                @include('partials.inputs.gallery-input')
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" id="submit-all">Save</button>
                    <a href="{{URL::route('portfolios')}}" class="btn btn-primary">Clear</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Example Basic Form -->
</div>
@include('partials.lists.portfolio-list')
@endsection()
