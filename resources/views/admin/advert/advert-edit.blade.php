<div class="col-sm-12">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Advert Form</h4>
        <div class="example">
            {!! Form::model($advert, array('method'=>'put', 'route'=>array('advert.update', $advert->id), 'files'=>true)) !!}
            @include('partials.inputs.advert.input')
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- End Example Basic Form -->
        <div class="clearfix"></div>
        @include('partials.lists.advert-list')
    </div>
    
</div>