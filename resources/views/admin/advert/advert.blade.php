@extends('layouts.admin')

@section('page-title', 'Adverts')

@section('panel-title', 'Manage  Adverts')

@section('content')
<div class="col-sm-12">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Advert Form</h4>
        <div class="example">
            {!! Form::open(array('route'=>'advert.save', 'files'=>true)) !!}
            @include('partials.inputs.advert-input')
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="{{URL::route('adverts')}}" class="btn btn-primary">Clear</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Example Basic Form -->
</div>
@include('partials.lists.advert-list')
@endsection()