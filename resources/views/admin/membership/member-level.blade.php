@extends('layouts.admin')

@section('page-title', 'Member Level')

@section('panel-title', 'Member Level')

@section('content')
<div class="col-sm-6">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Membership Level Form</h4>
        <div class="example">
            {!! Form::open(array('route'=>'packages.store')) !!}
                @include('partials.inputs.member-level-input')
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{URL::route('packages')}}" class="btn btn-info">Clear</a>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Example Basic Form -->
</div>
@include('partials.lists.member-level-list')
@endsection()