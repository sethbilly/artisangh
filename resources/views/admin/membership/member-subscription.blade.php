@extends('layouts.admin')

@section('page-title', 'Member Subscriptions')

@section('panel-title', 'Member Subscription')

@section('content')
<div class="col-sm-6">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Member Subscription Form</h4>
        <div class="example">
            <div class="row">
                {!! Form::open() !!}
                {{ csrf_field() }}
                <div class="form-inline col-sm-10 col-sm-offset-10">
                    <label>Search</label>
                    <input type="text" class="form-control"/>
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-12">
                    <div class="example-wrap">
                        <h4 class="example-title">Members</h4>
                        <div class="example">
                            <table>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Member ID</th>
                                </tr>
                                @forelse($members as $member)
                                <tr>
                                    <td><input type="checkbox"/></td>
                                    <td>$member->name</td>
                                    <td>$member->member_id</td>
                                </tr>
                                @empty
                                <p>No records found</p>
                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>



            <div class="row">
                {!! Form::open(array('route'=>'member-subscription-save')) !!}
                {{ csrf_field() }}
                <div class="form-inline col-sm-6">
                    <label class="control-label" for="paymentExpires">Payment Expires</label>
                    <input type="text" class="form-control" id="levelName" name="paymentExpires"
                           placeholder="Level Name"/>
                </div>
                <div class="form-inline col-sm-6">
                    <label class="control-label" for="paymentMade">Paid?</label>
                    <input type="checkbox" name="paymentMade" />
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Example Basic Form -->
</div>

<div class="col-sm-12">
    <!-- Example Basic Form Without Label -->
    <div class="example-wrap">
        <h4 class="example-title">Member Subscriptions</h4>
        <div class="example">
            <table class="table-responsive table-bordered table-primary table-striped">

            </table>
        </div>
    </div>
    <!-- End Example Basic Form Without Label -->
</div>
@endsection()