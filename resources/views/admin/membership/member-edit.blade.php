@extends('layouts.admin')

@section('page-title', 'Members')

@section('panel-title', 'Member Level')

@section('content')
<div class="col-sm-12">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Membership Form</h4>
        <div class="example">
            {!! Form::model($member, array('method'=>'put', 'route'=>array('member.update', $member->id) , 'files'=>true)) !!}
            @include('partials.inputs.member-input')
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                 <a href="{{URL::route('members')}}" class="btn btn-primary">Clear</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Example Basic Form -->
</div>
@include('partials.lists.member-list')


@endsection()