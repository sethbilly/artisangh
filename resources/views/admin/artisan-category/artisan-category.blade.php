@extends('layouts.admin')

@section('page-title', 'Artisan Categories')

@section('panel-title', 'Artisan Category')
@section('content')
<div class="col-sm-6">
    <!-- Example Basic Form -->
    <div class="example-wrap">
        <h4 class="example-title">Category Form</h4>
        <div class="example">
            <div class="row">
                {!! Form::open(array('route'=> 'artisan-category-save')) !!}
                @include('partials.inputs.artisan-category-input')
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- End Example Basic Form -->
</div>

<div class="col-sm-6">
    <!-- Example Basic Form Without Label -->
    <div class="example-wrap">
        <h4 class="example-title">Category List</h4>
        <div class="example">
            <!-- category table comes here -->
            <table class="table-responsive table-bordered table-striped">
                <tr>
                    <th>Category Name</th>
                    <th>Description</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                @foreach ($artisanCatList as $cat)
                <tr>
                    <td>{{ $cat->category_name }}</td>
                    <td>{{ $cat->description }}</td>
                    <td>
                        <a href="{{URL::route('artisan-category-edit', $cat->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Edit">
                            <span class="icon wb-edit"></span>
                        </a>
                    </td>
                    <td>
                        <a href="" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-trash"></span>
                        </a>
                    </td>

                </tr>
                @endforeach
            </table>
            {!! $artisanCatList->render() !!}
        </div>
    </div>
    <!-- End Example Basic Form Without Label -->
</div>
@endsection()