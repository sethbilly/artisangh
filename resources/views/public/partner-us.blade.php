@extends('layouts.public')

@section('slider')
@stop()

@section('main-content')
<div class="j-menu-container"></div>

<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Partner Us</span></li>
            </ul>
        </div>
    </div>
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">Partner Us</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-justify">
                    <p>
                        The Artisans Association of Ghana is a craftsmanship Association made up of numerous artisans.  It was established in January 2011 with over 300 members serving the Greater, Volta, Western and the Ashanti Regions. 
                    </p>
                    <p>
                        The association focuses on identifying projects with economic potential that can generate employment for as many artisans as possible.  Its vision and mission is to reduce youth unemployment in the informal sector, and get over 20,000 youth employed in the informal sector by 2020.
                    </p>
                    <p>
                        We will appreciate your support through donations or contributions to help train artisans through technology based programs or workshop and other skill related programs.
                    </p>
                    <p>
                        Partnership from companies and individuals are welcomed in organizing programs to help artisans in any way possible. This will go a long way in helping to get qualified and competent artisans in our nation thereby reducing unemployment in the informal sector. Thank you for your support.
                    </p>
                     <p>
                         Please be rest assured that donations of all forms when given to the AAG will be utilized for purposes for which it was acquired.
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
@stop()