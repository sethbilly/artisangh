<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Checkout</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="{{asset('public/index/wizard/css/bootstrap.min.css')}}" rel="stylesheet" >
        <link href="{{asset('public/index/wizard/css/gsdk-base.css')}}" rel="stylesheet" >

        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    </head>

    <body>
        <div class="image-container set-full-height" style="background-image: url({{URL::asset('public/index/wizard/imgs/register_bg.jpg')}})">
            <!--   Big container   -->
            <div class="container">
                <div class="row">
                    @include('flash::message')
                    <div class="col-sm-8 col-sm-offset-2">

                        <!--      Wizard container        -->   
                        <div class="wizard-container"> 
                            <div class="card wizard-card ct-wizard-orange" id="wizardProfile">
                                <div class="wizard-header">
                                    <h3>
                                        <b>PACKAGE PAYMENT<br>
                                            <small>Your Payment details</small>
                                    </h3>
                                </div>
                                <form action="https://manilla.nsano.com/checkout/" method="POST">
                                    <input type="hidden" name="currency" value="GHS" /> 
                                    <input type="hidden" name="return_url" value=" http://artisansghana.org/pfeedback-success/" /> 
                                    <input type="hidden" name="cancel_url" value="http://artisansghana.org/pfeedback-fail/" /> 
                                    <input type="hidden" name="merchant_id" value="300810865885" />
                                    <div class="row">
                                        <div class="col-sm-9 col-sm-offset-2">
                                            <div class="form-group">
                                                <label for="order_id">Order ID</label>
                                                <input type="text" name="order_id" value="{{ $order_id}}" readonly="true" class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="description">Item Description</label>
                                                <input type="text" name="description" value="{{$level->description}}" readonly="true" class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="amount">Amount</label>
                                                <input type="text" name="amount" value="{{$level->level_price}}" readonly="true" class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="cust_firstname">Customer Name</label>
                                                <input type="text" name="cust_firstname" value="{{$member_details->name}}" readonly="true" class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="cust_phone">Customer Phone</label>
                                                <input type="text" name="cust_phone" value="{{$member_details->telephone}}" readonly="true" class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="cust_city">Customer City</label>
                                                <input type="text" name="cust_city" value="{{$member_details->city}}" readonly="true" class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="country">Country</label>
                                                <input type="text" name="cust_country" value="Ghana" readonly="true" class="form-control"/>
                                            </div>

                                            <input type="submit" id="btnsub" value="Pay" class="btn btn-primary"/><br> 
                                        </div>
                                    </div>
                                </form>

                            </div>



                        </div>
                    </div> <!-- wizard container -->
                </div>
            </div><!-- end row -->
        </div> <!--  big container -->

        <div class="footer">
            <div class="container">

            </div>
        </div>


    </div>

</body>

<script src="{{asset('public/index/wizard/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
<script src="{{asset('public/index/wizard/js/bootstrap.min.js')}}" type="text/javascript"></script>

<!--   plugins 	 -->
<script src="{{asset('public/index/wizard/js/jquery.bootstrap.wizard.js')}}" type="text/javascript"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="{{asset('public/index/wizard/js/jquery.validate.min.js')}}"></script><!--

  methods for manipulating the wizard and the validation 
-->    <script src="{{asset('public/index/wizard/js/wizard.js')}}"></script>

</html>