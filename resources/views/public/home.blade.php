@extends('layouts.public')

@section('slider')
@include('partials.home-slider')
@stop

@section('main-content')
<br />
<section class="b-desc-section-container ">
    <div class=" b-portfolio-slider-box container b-carousel-title f-carousel-title f-carousel-title__color f-primary-b b-diagonal-line-bg-light" >
        @if(count($events) > 0)
        <div id="events">
            <ul>
                @foreach($events as $event)
                <li><strong>{{$event->title}}</strong>--{{$event->event_body}}</li>
                @endforeach
            </ul>
        </div>
        @else
            <marquee behavior="scroll" direction="left">No announcement or event available........</marquee>
        @endif
    </div>
    <div class="clearfix"></div> <br />
    <div class="container">

        <div class="row">
            <div class="col-md-3">
                <h2 class="f-center f-primary-b f-legacy-h2">Latest News</h2>
                <p class="b-desc-section f-desc-section f-center f-primary-l">Get our latest news</p>
                <div class="b-hr-stars f-hr-stars">
                    <div class="b-hr-stars__group">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
                @forelse($lastest_news as $news)
                <div class="row b-shortcode-example">
                    <div class="col-xs-12">
                        <div class="b-tagline-box b-tagline-box--big">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="{{asset('public/'.$news->thumbnail)}}" alt=""  width="100" height="100">
                                </div>

                                <div class="col-md-6">
                                    <i class="fa fa-calendar"></i>
                                    <span>{{$news->news_date}}</span>
                                </div>
                            </div>
                            <div class="b-tagline-box-inner">
                                <div class="b-tagline_title f-tagline_title f-primary-l"><a href="{{URL::route('news.detail', $news->id, $news->news_date)}}">{!! $news->title !!}</a></div>
                                <div class="f-tagline_description">
                                    {!! $news->summary !!}
                                </div>
                                <div class="b-tagline_btn b-tagline_btn">
                                    <a href="{{URL::route('news.detail', $news->id, $news->news_date)}}" class="button-sm">read more...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <p>No news available</p>
                @endforelse
                {!! $lastest_news->render() !!}
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="f-center f-primary-b f-legacy-h2">follow us </h2>
                        <p class="b-desc-section f-desc-section f-center f-primary-l">Get our lastest posts, tweets and videos</p>
                        <div class="b-hr-stars f-hr-stars">
                            <div class="b-hr-stars__group">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                        <div class="b-infoblock-with-icon-group row">

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="">
                                    <a href="https://www.facebook.com/artisans.gh" class="fade-in-animate" target="_blank">
                                        <img src="{{asset('public/index/images/social/facebook.png')}}" class="hvr-bounce-in"/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="b-infoblock-with-icon">
                                    <a href="https://plus.google.com/110998616301479852653/about" class="fade-in-animate" target="_blank">
                                        <img src="{{asset('public/index/images/social/g+.png')}}" class="hvr-bounce-in"/>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix visible-sm-block"></div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="b-infoblock-with-icon">
                                    <a href="https://twitter.com/artisan_gh" class="fade-in-animate" target="_blank">
                                        <img src="{{asset('public/index/images/social/twitter.jpg')}}" class="hvr-bounce-in"/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="b-infoblock-with-icon">
                                    <a href="https://www.youtube.com/channel/UCSKAxtfqGQ38o_7Z6RYJS4Q" class="fade-in-animate" target="_blank">
                                        <img src="{{asset('public/index/images/social/youtube.jpg')}}" class="hvr-bounce-in"/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="b-infoblock-with-icon">
                                    <a href="https://instagram.com/artisans.gh" class="fade-in-animate" target="_blank">
                                        <img src="{{asset('public/index/images/social/instagram.jpg')}}" class="hvr-bounce-in"/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="b-infoblock-with-icon">
                                    <a href="#" class="fade-in-animate tooltip">

                                        <img src="{{asset('public/index/images/social/whatsapp.png')}}" class="hvr-bounce-in"/>
                                        <span>
                                            <strong>WhatsApp us on 0501524697</strong>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix visible-md-block visible-lg-block"></div>

                            <div class="clearfix visible-sm-block"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-md-block visible-lg-block"></div>
                <div class="clearfix visible-md-block visible-lg-block"></div>
                <br /><br/><br /><br/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="b-slidercontainer b-slider" style="height: 400px;">
                            <div class="j-fullscreenslider">
                                <ul>
                                    <!-- Slide 1 -->
                                    <li data-transition="3dcurtain-vertical" data-slotamount="7" >
                                        <div class="tp-bannertimer"></div>
                                        <!-- Main Image -->
                                        <img data-retina src="{{asset('public/index/images/slider/slide_img7.jpg')}}">
                                        <!-- Layer 1 -->
                                        <div class="caption sfr"  data-x="center" data-y="100" data-speed="600" data-start="2600">
                                            <h1 class="f-primary-b c-white f-legacy-h1" >
                                                Are you tired of the others? Welcome here
                                            </h1>
                                        </div>
                                        <div class="caption"  data-x="center" data-y="170" data-speed="600" data-start="3200">
                                            <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
                                                <a href="{{URL::route('search.artisans')}}" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b">search artisans</a>
                                                <span class="b-slider-lg-info-l__item-link-after"><i class="fa fa-chevron-right"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- Slide 2 -->
                                    <li data-transition="" data-slotamount="7">
                                        <div class="tp-bannertimer"></div>
                                        <img data-retina src="{{asset('public/index/images/slider/slide_img8.jpg')}}">
                                        <div class="caption sfr"  data-x="center" data-y="100" data-speed="600" data-start="2600">
                                            <h1 class="f-primary-b c-white f-legacy-h1" >We have the people that will get <br> your job done</h1>
                                        </div>
                                        <div class="caption"  data-x="center" data-y="250" data-speed="600" data-start="3200">
                                            <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
                                                <a href="{{URL::route('search.artisans')}}" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b">search artisans</a>
                                                <span class="b-slider-lg-info-l__item-link-after"><i class="fa fa-chevron-right"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- Slide 3 -->
                                    <li data-transition="" data-slotamount="7">
                                        <div class="tp-bannertimer"></div>
                                        <img data-retina src="{{asset('public/index/images/slider/slide_img9.jpg')}}">

                                        <div class="caption lft"  data-x="center" data-y="110" data-speed="600" data-start="2600">
                                            <h1 class="f-primary-b c-white f-legacy-h1" >We have qualified and certified artisans</h1>
                                        </div>
                                        <div class="caption"  data-x="center" data-y="170" data-speed="600" data-start="3200">
                                            <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
                                                <a href="{{URL::route('search.artisans')}}" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b">search artisans</a>
                                                <span class="b-slider-lg-info-l__item-link-after"><i class="fa fa-chevron-right"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-3">
                <div class="type-attachment">
                    @if(count($adverts) > 0)
                    <div id="advert" >
                        <ul>
                            @foreach($adverts as $advert)
                            <li><img src="{{asset('public/'.$advert->feature_image)}}"</li>
                            @endforeach
                        </ul>
                    </div>
                    @else
                    <h2>Advertise Here</h2>
                    <p>
                        Contact us to place your adverts here
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection()