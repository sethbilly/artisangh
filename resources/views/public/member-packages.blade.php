@extends('layouts.public')

@section('main-content')
<div class="b-breadcrumbs f-breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
            <li><i class="fa fa-angle-right"></i><span>Member Packages</span></li>
        </ul>
    </div>
</div>
<div class="container">

    <div class="b-pricing-info__container f-center">
        <div class="row">
            
            <div class="col-md-12">
                <div class="b-pricing-info__item f-pricing-info__item col-sm-6  col-xs-12">
                    <div class="b-pricing-info__item-row b-pricing-info__item-title">
                        <h4 class="f-legacy-h4 f-primary-b">Standard Package</h4>
                    </div>
                    <div class="b-pricing-info__item-row b-pricing-info__item-price">
                        <h2 class="f-legacy-h2 f-primary-b">GHC 30 / <small class="f-primary">year</small></h2>
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Link To Jobs
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-times"></i> Link To Contracts
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Membership Card
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> T-Shirts
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Training
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-times"></i> Free Online Listing(AAG Website)
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Bank Account(Optional)
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-times"></i> Business Advisory Services
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-times"></i> Finance Access
                    </div>
                    <div class="b-pricing-info__item-row b-pricing-info__item-btn-row">
                        <a class="b-btn f-btn b-btn-sm f-primary-b" href="{{URL::route('register')}}">Register</a>
                    </div>
                </div>

                <div class="clearfix visible-sm-block"></div>

                <div class="b-pricing-info__item f-pricing-info__item col-sm-6  col-xs-12">
                    <div class="b-pricing-info__item-row b-pricing-info__item-title">
                        <h4 class="f-legacy-h4 f-primary-b">Premium Package</h4>
                    </div>
                    <div class="b-pricing-info__item-row b-pricing-info__item-price">
                        <h2 class="f-legacy-h2 f-primary-b">GHC 60 / <small class="f-primary">year</small></h2>
                    </div>

                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Link To Jobs
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Link To Contracts
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Membership Card 
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> T-Shirts
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Training
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Free Online Listing(AAG Website)
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Bank Account(Optional)
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Business Advisory Services
                    </div>
                    <div class="b-pricing-info__item-row">
                        <i class="fa fa-check"></i> Finance Access
                    </div>
                    <div class="b-pricing-info__item-row b-pricing-info__item-btn-row">
                        <a class="b-btn f-btn b-btn-sm f-primary-b" href="{{URL::route('register')}}">Register</a>
                    </div>
                </div>
            </div>
           
        </div>

    </div>
</div>
@stop()