@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<section class="b-desc-section-container b-diagonal-line-bg-light">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Partners</span></li>
                <li><i class="fa fa-angle-right"></i><span>HFC Boafo</span></li>
            </ul>
        </div>
    </div>
    
    <div class="container">
        <h2 class="f-center f-primary-b f-legacy-h2">hfc boafo</h2>
        <div class="b-hr-stars f-hr-stars">
            <div class="b-hr-stars__group">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img alt="yesgh" src="{{asset('public/index/images/logos/hfcboafo.png')}}"/>
            </div>
            <div class="col-md-8 text-justify">
                <p>
                    OICG targets literate youth with two-year classroom-based training programs in English in 14 areas including electrical, carpentry, masonry, drafting, and welding and fabrication. OICG also targets non- and semi-literate youth with an apprenticeship program that includes basic literacy and numeracy, plus 6-12 month on-the-job technical training using pictorial manuals and local languages.
                </p>
                <p>
                    OICG partners with close to 80 master craftspeople to improve their ability to train and mentor the apprentices. Both of OICG’s training models incorporate recruitment, screening and orientation, as well as basic business management and life skills, career counseling, job-placement and follow-up services.
                </p>
            </div>
        </div>
    </div>
</section>
@endsection()