@extends('layouts.public')
@section('slider')
@endsection()
@section('main-content')
<section class="b-desc-section-container b-diagonal-line-bg-light">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Partners</span></li>
                <li><i class="fa fa-angle-right"></i><span>YES GH</span></li>
            </ul>
        </div>
    </div>
    
    <div class="container">
        <h2 class="f-center f-primary-b f-legacy-h2">yes gh</h2>
        <div class="b-hr-stars f-hr-stars">
            <div class="b-hr-stars__group">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img alt="yesgh" src="{{asset('public/index/images/logos/yesgh.png')}}"/>
            </div>
            <div class="col-md-8 text-justify">
                <p>
                    Youth Empowerment Synergy (YES-Ghana) is a youth-based NGO formed in 2001. Its key competencies are youth employment and entrepreneurial development, youth leadership and capacity development, and youth-centered policy and advocacy.
                </p>
                <p>
                    With approximately 450,000 youth members across all ten regions, YES-Ghana has become Ghana’s foremost youth organization. YES-Ghana also manages the Voices of Youth Coalition, a youth-led advocacy platform to engage youth in shaping the development agenda and raise awareness of issues affecting youth.                   
                </p>
            </div>
        </div>
    </div>
</section>

@endsection()