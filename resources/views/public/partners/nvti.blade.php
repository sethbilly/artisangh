@extends('layouts.public')

@section('slider')
@stop()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Partners</span></li>
                <li><i class="fa fa-angle-right"></i><span>NVTI</span></li>
            </ul>
        </div>
    </div>
    
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">NVTI</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="row">
<!--                <div class="col-md-4">
                    
                </div>-->
                
                <div class="col-md-12 text-justify">
                    <p>
                        National Vocational Training Institute (NVIT) is a national body set up by an Act of Parliament 351 of the Republic of Ghana to co-ordinate and  
                        NVTI provides the best systems of employable TVET Skills. Our mission is to provide demand-driven employable skills and enhance the income generating capacities of basic and secondary school leavers, and such other persons through Competency-Based Apprenticeship, Master Craftsmanship, Testing and Career Development. 
                    </p>
                    <p>
                        NVTI believes in Pursuit of excellence, Teamwork, Respect for all, Truth, honesty, integrity and Cost-effectiveness.
                    </p>
                    <p>
                        NVTI strive for effective collaboration between other departments/agencies to improve vocational and technical training in the country.
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
@stop()
