@extends('layouts.public')

@section('slider')
@stop()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Partners</span></li>
                <li><i class="fa fa-angle-right"></i><span>COTVET</span></li>
            </ul>
        </div>
    </div>
    
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">cotvet</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="row">
<!--                <div class="col-md-4">
                    
                </div>-->
                
                <div class="col-md-12 text-justify">
                    <p>
                        Council for Technical and Vocational Education and Training (COTVET) is a national body set up by an Act of Parliament of the Republic of Ghana to co-ordinate and oversee all aspects of technical and vocational education and training in the country. Our major objective is to formulate policies for skills development across the broad spectrum of pre-tertiary and tertiary education, formal, informal and non-formal sectors.
                    </p>
                    <p>
                        COTVET is establishing Ghana's Technical Vocational Education and Training (TVET) system to improve the productivity and competitiveness of the skilled workforce and raise the income generating capacities of people, especially women and low income groups through provision of quality-oriented, industry-focused and competency-based training programs and complementary services.
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
@stop()
