@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Partners</span></li>
                <li><i class="fa fa-angle-right"></i><span>Global Communities</span></li>
            </ul>
        </div>
    </div>
    
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">global communities</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <img alt="aabn" src="{{asset('public/index/images/logos/globalc.png')}}"/>
                </div>
                <div class="col-md-8 text-justify">
                    <p>
                        Global Communities is a US-based international non-profit organization. Its mission is to create long-lasting, positive and community-led change that improves the lives and livelihoods of vulnerable people across the globe. The organization believes that the people who understand their needs best are the people of the community themselves. In Ghana from 2009 to 2013, Global Communities generated formal employment and entrepreneurship opportunities for low-income youth in the solid waste sector under the Youth Engagement in Service Delivery (YES) project.
                    </p>
                    <p>
                        Working with Artisans association and other four consortium members under the Youth Inclusive Entrepreneurial Development Initiative for Employment (YIEDIE) will replicate with the construction sector the value chain approach they successfully used under YES.
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection()
