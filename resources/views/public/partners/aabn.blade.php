@extends('layouts.public')

@section('slider')
@stop()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Partners</span></li>
                <li><i class="fa fa-angle-right"></i><span>AABN</span></li>
            </ul>
        </div>
    </div>
    
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">aabn</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <img alt="aabn" src="{{asset('public/index/images/logos/aabn.png')}}"/>
                </div>
                <div class="col-md-8 text-justify">
                    <p>
                        African Aurora Business Network LLC (AABN) is a market-oriented enterprise development organization established in 2001 with a record of successful project management and service delivery. The partnership with AABN would enable AAG members to start a new business or expand and formalize an existing business.
                    </p>
                    <p>
                        It will also take advantage of market opportunities to support entrepreneurial master craftspeople or other SMEs that are prepared to expand and hire more youth apprentices or workers, or source raw materials or services from participants in entrepreneurship training. AABN will link women participants to women’s business networks for additional support and networking.
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
@stop()