<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Register</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="{{asset('public/index/wizard/css/bootstrap.min.css')}}" rel="stylesheet" >
        <link href="{{asset('public/index/wizard/css/gsdk-base.css')}}" rel="stylesheet" >

        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    </head>

    <body>
        <div class="image-container set-full-height" style="background-image: url({{URL::asset('public/index/wizard/imgs/register_bg.jpg')}})">
            <!--   Big container   -->
            <div class="container">
                @include('flash::message')
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!--      Wizard container        -->   
                        <div class="wizard-container"> 
                            <div class="clearfix"></div>
                            <div class="card wizard-card ct-wizard-orange" id="wizardProfile">
                                <div class="wizard-header">
                                    <h3>

                                    </h3>
                                </div>
                                <div class="row">
                                    <div class="col-md-7 col-md-offset-4">
                                        @if(isset($checkout_data))
                                        <b>Click here</b>  <a href="{{URL::route('checkout', $checkout_data)}}" class="btn btn-success">pay</a>
                                        @else
                                        <b>Sorry! An error occured. Please try</b><a href="{{URL::route('register')}}" class="btn btn-warning">again</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div> <!-- wizard container -->
                    </div>
                </div><!-- end row -->
            </div> <!--  big container -->

            <div class="footer">
                <div class="container">

                </div>
            </div>


        </div>

    </body>

    <script src="{{asset('public/index/wizard/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/index/wizard/js/bootstrap.min.js')}}" type="text/javascript"></script>
</html>
