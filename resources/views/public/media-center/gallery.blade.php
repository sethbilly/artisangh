@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Media Center</span></li>
                <li><i class="fa fa-angle-right"></i><span>Gallery</span></li>
            </ul>
        </div>
    </div>
    <div class="j-filter-content">

        <div class="row b-portfolio-gallery j-masonry">
            <div class="masonry-gridSizer col-md-4 col-sm-6 col-xs-12"></div>
            @foreach($portfolios as $portfolio)
            <div class="j-masonry-item col-md-4 col-sm-6 col-xs-12 j-filter-graphic j-filter-web j-filter-mob">
                <div>
                    <div class="b-app-with-img__item">
                        <div class="b-app-with-img__item_img view view-sixth">
                            <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/portfolio/video-pic6.jpg" alt=""/></a>
                            <div class="b-item-hover-action f-center mask">
                                <div class="b-item-hover-action__inner">
                                    <div class="b-item-hover-action__inner-btn_group">
                                        <a href="" class="b-btn f-btn b-btn-light f-btn-light info fancybox fancybox.iframe" title="Mobile games" rel="group2"><i class="fa fa-arrows-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-app-with-img__item_text f-center b-app-with-img__border">
                            <div class="b-app-with-img__item_name f-app-with-img__item_name f-primary-b"><a href="javascript:;">Mobile games</a></div>
                            <div class="b-app-with-img__item_info f-app-with-img__item_info f-primary">Mobile Design, 3D Graphic</div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
           
        </div>
    </div>


</div>
@endsection()
