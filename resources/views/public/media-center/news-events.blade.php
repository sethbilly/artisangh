@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Media Center</span></li>
                <li><i class="fa fa-angle-right"></i><span>News & Events</span></li>
            </ul>
        </div>
    </div>
    <section class="b-portfolio-slider-box container">
        <div class="container">
            <div class="b-carousel-title f-carousel-title f-carousel-title__color f-primary-b b-diagonal-line-bg-light">
                News And Events
            </div>
            <div class="j-filter-content">

                <div class="row b-portfolio-gallery j-masonry">
                    <div class="masonry-gridSizer col-md-4 col-sm-6 col-xs-12"></div>
                    @foreach($news as $news_details)
                    <div class="j-masonry-item col-md-4 col-sm-6 col-xs-12 j-filter-graphic j-filter-web j-filter-mob">
                        <div>
                            <div class="b-app-with-img__item">
                                <div class="b-app-with-img__item_img view view-sixth">
                                    <a href="{{URL::route('news.detail', $news_details->id)}}"><img class="j-data-element" data-animate="fadeInDown" data-retina src="{{asset('public/'.$news_details->feature_image)}}" alt="" height="350" width="120"/></a>                                           
                                    <div class="b-app-with-img__item_text f-center b-app-with-img__border">
                                        <div class="b-app-with-img__item_name f-app-with-img__item_name f-primary-b"><a href="{{URL::route('news.detail', $news_details->id)}}">{!! $news_details->title !!}</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <br />
                </div>
            </div>
            {!! $news->render() !!}
    </section>
</div>
@endsection()