@extends('layouts.public')

@section('slider')
@stop()

@section('main-content')
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>News</span></li>
                <li><i class="fa fa-angle-right"></i><span>Detail</span></li>
            </ul>
        </div>
    </div>
    <div class="container l-inner-page-container">
        <div class="b-portfolio-box col-md-12">
            <div class="b-portfolio-item">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="b-portfolio_image">
                            <div class="b-portfolio_image_box view view-sixth">
                                <img data-retina="" src="{{asset('public/'.$news->feature_image)}}" alt="">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="b-portfolio_info">
                            <div class="b-portfolio_info_title f-primary-b f-portfolio_info_title">
                                <a href="#">{!! $news->title !!}</a>
                            </div>
                            <div class="b-portfolio_info_rating">
                                <span class="b-rating_bord hidden-xs"></span>
                                <div class="b-portfolio_rating_category">
                                    <i class="fa fa-calendar"></i>
                                    <span>{{$news->news_date}}</span>
                                </div>
                            </div>
                            <div class="b-portfolio_info_description f-portfolio_info_description">
                                <span>{!! $news->body !!}</span>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop()