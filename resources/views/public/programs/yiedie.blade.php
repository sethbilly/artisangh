@extends('layouts.public')

@section('main-content')
<div class="l-main-container" >
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Programs</span></li>
                <li><i class="fa fa-angle-right"></i><span>YEDIE</span></li>
            </ul>
        </div>
    </div>

    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">yiedie</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-4">
                    <img alt="yiedie" src="{{asset('public/index/images/logos/yiedie.png')}}"/>
                </div>
                <div class="col-md-8 text-justify">
                    <p>
                        The Youth Inclusive Entrepreneurial Development Initiative for Employment (YIEDIE) is designed to create economic opportunities in Ghana’s construction sector for disadvantaged youth. YIEDIE will directly reach at least 23,700 of these youth with training in technical, life and/or entrepreneurship skills leading to employment. As a result of the project, these youth will increase their income and savings and create new job opportunities in the sector, thereby indirectly benefiting an additional 112,550 people.
                    </p>
                    <p>
                        YIEDIE means “progress” in the Twi language. The project will apply an integrated youth-led market-systems model to improve the capacity of youth and service providers across the value chain. Youth will benefit from having job opportunities and higher income. Sector stakeholders—such as private sector firms, financial service providers, training institutions and government—are expected to benefit from improved coordination and capacity.                   
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection()