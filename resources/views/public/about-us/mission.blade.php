@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>About Us</span></li>
                <li><i class="fa fa-angle-right"></i><span>Mission</span></li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
     <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">mission</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <p>
                To get over 20,000 youth employed in the informal sector by 2020.
            </p>
        </div>
    </section>
</div>
@endsection()