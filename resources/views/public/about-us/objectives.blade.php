@extends('layouts.public')

@section('slider')
@stop()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>About Us</span></li>
                <li><i class="fa fa-angle-right"></i><span>Mission</span></li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">objectives</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <p>
                <i class="fa fa-tag" ></i>To identify projects with economic potential that can generate employment for as many artisans as possible.
            </p>
            <p>
                <i class="fa fa-tag"></i>To upgrade artisans with new and innovative technologies and as well expose them to the job market.
            </p>
            <p>
                <i class="fa fa-tag"></i>Train and build interest in the youth to become artisans.
            </p>
            <p>
                <i class="fa fa-tag"></i>Collaborate with other organizations both locally and internationally, district and municipal assemblies, ministries and other private entities to source for funds for the youth to start up their business.
            </p>
            <p>
                <i class="fa fa-tag"></i>Bring unity among all artisans in the country.
            </p>
        </div>
    </section>
</div>
@stop()