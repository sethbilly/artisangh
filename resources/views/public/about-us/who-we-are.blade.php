@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>About Us</span></li>
                <li><i class="fa fa-angle-right"></i><span>Who We Are</span></li>
            </ul>
        </div>
    </div>

    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">who we are</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-8 text-justify">
                    <aside>
                        <h4 class="f-primary-b b-h4-special f-h4-special c-primary">about us</h4>
                        <p>
                            The Artisans Association of Ghana (AAG) is a craftsmanship Association made up of masons, carpenters, electricians, welders, plumbers, glass and metal fabricators, steel benders, Auto mechanics, plaster of Paris (POP), etc.
                        </p>
                        <p>
                            AAG, registered on the 27th of November, currently serves the Greater Accra, Western, Volta and Ashanti Region, and has plans of expanding nationwide. It works with national level stakeholders and has an affiliation with NVTI and also connects its members to national level services such as the National Health Insurance Scheme (NHIS).
                        </p>
                    </aside>
                </div>
                <div class="col-md-4">
                    <aside>
                        <h4 class="f-primary-b b-h4-special f-h4-special c-primary">our philosophy</h4>
                        <div class="b-tagline-box animated swing">
                            <div class="b-tagline-box-inner" >
                                <div class="f-tagline_description ">
                                    Every individual is unique and endowed with potentials, gifts and talents which need to be unearthed.
                                </div>
                            </div>
                        </div>
                        <h4 class="f-primary-b b-h4-special f-h4-special c-primary">core values</h4>
                        <div class="animated rubberBand">
                            <p >Unity</p>
                            <p >Integrity</p>
                            <p >Honesty</p>
                            <p >Teamwork</p>
                            <p >Truth</p>
                            <p >Respect for all</p>
                        </div>

                    </aside>
                </div>
                <div class="col-md-12">
                    <section class="b-infoblock">
                        <div class="container">
                            <h2 class="f-legacy-h2 f-primary-b f-center">Meet our board</h2>
                            <p class="f-center c-s-62 f-title-small"></p>
                            <div class="b-shortcode-example">
                                <div class="b-employee-container row">
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <div class="b-employee-item b-employee-item--color f-employee-item">
                                            <div class=" view view-sixth">
                                                <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="{{asset('public/index/images/board-members/emma_korsah.jpg')}}" alt="" style="height: 255px"/></a>
                                                <div class="b-item-hover-action f-center mask">
                                                    <div class="b-item-hover-action__inner">
                                                        <div class="b-item-hover-action__inner-btn_group">
                                                            <a href="about_us_meet_our_team_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="f-primary-b">Emmanuel Korsah</h4>
                                            <div class="b-employee-item__position f-employee-item__position">(C.E.O) Akan Centre Furnisher Company Limited</div>
                                            <p></p>
                                            <div class="b-employee-item__social">
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-google-plus"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-thumbs-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <div class="b-employee-item b-employee-item--color f-employee-item">
                                            <div class=" view view-sixth">
                                                <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="{{asset('public/index/images/board-members/salomey_aku.png')}}" alt=""/></a>
                                                <div class="b-item-hover-action f-center mask">
                                                    <div class="b-item-hover-action__inner">
                                                        <div class="b-item-hover-action__inner-btn_group">
                                                            <a href="about_us_meet_our_team_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="f-primary-b">Salomey Aku Ackom</h4>
                                            <div class="b-employee-item__position f-employee-item__position">Vice Principal of Youth Leadership and Skills Training Institute, Afienya. </div>
                                            <p></p>
                                            <div class="b-employee-item__social">
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-google-plus"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-thumbs-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <div class="b-employee-item b-employee-item--color f-employee-item">
                                            <div class=" view view-sixth">
                                                <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="{{asset('public/index/images/board-members/pastor_drah.png')}}" alt=""/></a>
                                                <div class="b-item-hover-action f-center mask">
                                                    <div class="b-item-hover-action__inner">
                                                        <div class="b-item-hover-action__inner-btn_group">
                                                            <a href="about_us_meet_our_team_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="f-primary-b">Pastor Charles Drah</h4>
                                            <div class="b-employee-item__position f-employee-item__position">Former Squadron Leader</div>
                                            <p></p>
                                            <div class="b-employee-item__social">
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-google-plus"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-thumbs-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-xs-12 hidden-xs hidden-sm">
                                        <div class="b-employee-item b-employee-item--color f-employee-item">
                                            <div class=" view view-sixth">
                                                <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="{{asset('public/index/wizard/imgs/default-avatar.png')}}" alt=""/></a>
                                                <div class="b-item-hover-action f-center mask">
                                                    <div class="b-item-hover-action__inner">
                                                        <div class="b-item-hover-action__inner-btn_group">
                                                            <a href="about_us_meet_our_team_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="f-primary-b">Nii Annang Adzor</h4>
                                            <div class="b-employee-item__position f-employee-item__position">C.E.O) Millennium Construction Company Limited</div>
                                            <p></p>
                                            <div class="b-employee-item__social">
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-google-plus"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-thumbs-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="b-shortcode-example">
                                <div class="b-employee-container row">
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <div class="b-employee-item b-employee-item--color f-employee-item">
                                            <div class=" view view-sixth">
                                                <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="{{asset('public/index/wizard/imgs/default-avatar.png')}}" alt=""/></a>
                                                <div class="b-item-hover-action f-center mask">
                                                    <div class="b-item-hover-action__inner">
                                                        <div class="b-item-hover-action__inner-btn_group">
                                                            <a href="about_us_meet_our_team_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="f-primary-b">Mrs. Augusta Baafi Borbordzi</h4>
                                            <div class="b-employee-item__position f-employee-item__position">Journalist</div>
                                            <p></p>
                                            <div class="b-employee-item__social">
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-twitter"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-google-plus"></i></a>
                                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-thumbs-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection()
