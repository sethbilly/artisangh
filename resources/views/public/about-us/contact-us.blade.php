@extends('layouts.public')

@section('slider')
@endsection()
@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><a href="#">About us</a></li>
                <li><i class="fa fa-angle-right"></i><span>Contact Us</span></li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">contact us</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
        </div>
    </section>
    <section class="b-google-map map-theme b-bord-box" data-location-set="contact-us">
        <div class="b-google-map__map-view b-google-map__map-height">
            <!-- Google map load -->
        </div>
<!--        <img data-retina src="img/google-map-marker-default.png" data-label="" class="marker-template hidden" />-->
        <div class="b-google-map__info-window-template hidden"
             data-selected-marker="0"
             data-width="250">
            <div class="b-google-map__info-window f-center b-google-map__info-office f-google-map__info-office">
                <h4 class="f-primary-b">Artisans Association Of Ghana</h4>
                <small>Office</small>
            </div>
        </div>
    </section>
    <div class="b-desc-section-container">
        <section class="container b-welcome-box">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <h1 class="is-global-title f-center f-legacy-h1">We’d love to hear from you!</h1>
                    <p class="f-center">
                        Get in touch with us today!
                    </p>
                </div>
            </div>
        </section>
        <section class="container">
            <div class="row">
                <div class="col-sm-6 b-contact-form-box">
                    <h3 class="f-legacy-h3 f-primary-b b-title-description f-title-description">
                        Send us a message
                        <div class="b-title-description__comment f-title-description__comment f-primary-l"></div>
                    </h3>
                    <div class="row">
                        <form action="#" method="post">
                            <div class="col-md-6">
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="name">Your name</label>
                                    <div class="b-form-vertical__input">
                                        <input type="text" id="name" class="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="email">You email</label>
                                    <div class="b-form-vertical__input">
                                        <input type="text" id="email" class="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="website">Your website</label>
                                    <div class="b-form-vertical__input">
                                        <input type="text" id="website" class="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="title">Your title</label>
                                    <div class="b-form-vertical__input">
                                        <input type="text" id="title" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="b-form-row b-form--contact-size">
                                    <label class="b-form-vertical__label">Your message</label>
                                    <textarea class="form-control" rows="5"></textarea>
                                </div>
                                <div class="b-form-row">
                                    <a href="#" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100">send message</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 b-contact-form-box">
                    <h3 class="f-legacy-h3 f-primary-b b-title-description f-title-description">
                        contact info
                        <div class="b-title-description__comment f-title-description__comment f-primary-l"></div>
                    </h3>
                    <div class="row b-google-map__info-window-address">
                        <ul class="list-unstyled">
                            <li class="col-xs-12">
                                <div class="b-google-map__info-window-address-icon f-center pull-left">
                                    <i class="fa fa-home"></i>
                                </div>
                                <div>
                                    <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                        Artisans Association Of Ghana
                                    </div>
                                    <div class="desc">H/No. 3 Afariwa Box TV 600 Tema Newtown</div>
                                </div>
                            </li>
                            <li class="col-xs-12">
                                <div class="b-google-map__info-window-address-icon f-center pull-left">
                                    <i class="fa fa-skype"></i>
                                </div>
                                <div>
                                    <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                        Skype
                                    </div>
                                    <div class="desc">ask.company</div>
                                </div>
                            </li>
                            <li class="col-xs-12">
                                <div class="b-google-map__info-window-address-icon f-center pull-left">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div>
                                    <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                        email
                                    </div>
                                    <div class="desc">info@artisansghana.org</div>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection()
