@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>FAQs</span></li>
            </ul>
        </div>
    </div>
    <section class="b-infoblock--small">
        <div class="container">
            <div class="row b-col-default-indent">
                <div class="col-md-9">
                    <p class="f-primary-b f-uppercase f-title-small c-primary">Frequently asked questions</p>

                    <div class="j-accordion b-accordion f-accordion b-accordion--secondary b-accordion--with-icon-indent f-accordion--secondary">
                        <h3 class="b-accordion__title f-accordion__title f-primary-b"><i
                                class="fa fa-question-circle"></i> How do I become a member of Artisans Association of Ghana?
                        </h3>

                        <div class="b-accordion__content">
                            <p><i class="fa fa-tag" ></i>To become a full member, AAG requires that applicants have some kind of technical skills and register with a fee.</p>
                        </div>
                        <h3 class="b-accordion__title f-accordion__title f-primary-b"><i
                                class="fa fa-question-circle"></i>What benefits do i gain as an Artisan?</h3>

                        <div class="b-accordion__content">
                            <p><i class="fa fa-tag" ></i>Securing jobs or contracts for all.</p>
                            <p><i class="fa fa-tag" ></i>AAG advocates for the interest of all artisans in the construction industry.</p>
                            <p><i class="fa fa-tag" ></i>Job placement on the Association’s website.</p>
                            <p><i class="fa fa-tag" ></i>To provide legal aid in times of problems.</p>
                            <p><i class="fa fa-tag" ></i>Artisans can contact each other for assistance.</p>
                            <p><i class="fa fa-tag" ></i>Upgrade the master craft persons</p>
                            <p><i class="fa fa-tag" ></i>Certification of members after training with Locally and Internationally Recognized Institutions.</p>
                            <p><i class="fa fa-tag" ></i>Train Artisans on new technology on their field.</p>
                        </div>
                        <h3 class="b-accordion__title f-accordion__title f-primary-b"><i
                                class="fa fa-question-circle"></i>What benefits do i gain as a Stakeholders (our Partner, Client, Household, Estate Developer, Government, etc)? </h3>

                        <div class="b-accordion__content">
                            <p><i class="fa fa-tag" ></i>Home/Industry services e.g.</p>
                            <p><i class="fa fa-tag" ></i>Qualified and competent Labour.</p>
                            <p><i class="fa fa-tag" ></i>Moderate and affordable Labour.</p>
                            <p><i class="fa fa-tag" ></i>24 hour service.</p>
                            <p><i class="fa fa-tag" ></i>Protection of materials and equipment from getting missing.</p>
                            <p><i class="fa fa-tag" ></i>Effective cost estimation.</p>
                        </div>
                    </div>

                    <section
                        class="b-infoblock-with-icon-group b-infoblock-with-icon--small-indent row b-infoblock-with-icon--sm f-infoblock-with-icon--sm b-infoblock-with-icon--biggest-icons b-infoblock-with-icon--center f-infoblock-with-icon--center b-default-top-indent">
                        <div class="col-md-4 col-sm-6 col-xs-12 b-null-bottom-indent">
                            <div class="b-infoblock-with-icon">
                                <a href="contact_us_v2.html"
                                   class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
                                    <i class="fa fa-comments"></i>
                                </a>

                                <div class="b-infoblock-with-icon__info f-infoblock-with-icon__info">
                                    <a href="#"
                                       class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-title-big f-primary-sb">Live
                                        chat with us</a>

                                    <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                                        Chat with us on our Whatsapp number. Get quick and immediate response from our team
                                        who are always at hand to lend the help you need.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 b-null-bottom-indent">
                            <div class="b-infoblock-with-icon">
                                <a href="#"
                                   class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
                                    <i class="fa fa-headphones"></i>
                                </a>

                                <div class="b-infoblock-with-icon__info f-infoblock-with-icon__info">
                                    <a href="contact_us_v2.html"
                                       class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-title-big f-primary-sb">Free
                                        call for all</a>

                                    <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                                        Call us for further inquiries and support
                                    </div>

                                </div>
                            </div>
                        </div>

                    </section>

                </div>
                <aside class="col-md-3">    
                    <div class="b-default-top-indent">
                        <h4 class="f-primary-b b-h4-special f-h4-special c-primary">submit your questions</h4>

                        <div class="b-form-row">
                            <input type="text" class="form-control" placeholder="Your email"/>
                        </div>
                        <div class="b-form-row">
                            <input type="text" class="form-control" placeholder="Subject"/>
                        </div>
                        <div class="b-form-row">
                            <textarea class="form-control" placeholder="Your questions" rows="5"></textarea>
                        </div>
                        <div class="b-form-row">
                            <a href="#" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100">submit
                                questions</a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>

</div>
@endsection()