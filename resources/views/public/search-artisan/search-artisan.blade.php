@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Search Artisans</span></li>
            </ul>
        </div>
    </div>
    <section class="b-full-search">
        <div class="container">
            <div class="b-full-search-form">
                    
                    <div class="row">
                    {!! Form::open(array('route'=>'search.artisans', 'method' =>'GET')) !!}
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 b-full-search-form_col">
                            <div class="b-full-search-form_title f-full-search-form_title f-primary-b">Category?</div>
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::select('category', [''=> 'Select Category'] + $categories, null , array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 b-full-search-form_col">
                            <div class="b-full-search-form_title f-full-search-form_title f-primary-b">Where?</div>
                            <div row>
                                <div class="form-inline col-md-4">
                                    {!! Form::select('region', [''=> 'Select Region'] + $regions, null , array('class' => 'form-control')) !!}
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="city..." name="city">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="town..." name="town">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix visible-md visible-sm"></div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 b-full-search-form_col">
                            <div class="b-full-search-form_title f-full-search-form_title f-primary-b">Go?</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button class="b-search-map__submit b-btn f-btn b-btn-light f-btn-light f-primary-b f-left"><i class="fa fa-search"></i> search</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::token() !!}
                        {!! Form::close() !!}
                    </div>
            </div>
            <div class="b-search-title f-search-title f-primary-b">{{$artisans->total()}} artisans found</div>
        </div>
    </section>
    <div class="container">
        <div class="l-inner-page-container">
            <div class="row">
                @foreach($artisans as $artisan)
                <div class="col-md-4 col-sm-6 col-xs-12 b-item-apartment-block">
                    <div class="b-some-examples__item f-some-examples__item">
                        <div class="b-some-examples__item_img view view-sixth">
                            @if(isset($artisan->member_picture))
                             <img data-retina="" src="{{asset('public/'.$artisan->member_picture)}}">
                            @else
                            <img data-retina="" src="{{asset('public/index/wizard/imgs/default-avatar.png')}}" >
                            @endif
                            <div class="b-item-hover-action f-center mask">
                                <div class="b-item-hover-action__inner">
                                    <div class="b-item-hover-action__inner-btn_group">
                                        <a href="{{URL::route('artisan.detail',$artisan->id)}}" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-some-examples__item_info">
                            
                            <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-primary-b"><a href="{{URL::route('artisan.detail',$artisan->id)}}">{{$artisan->name}}</a></div>
                            <div class="b-some-examples__item_info_level b-some-examples__item_double_info f-some-examples__item_double_info clearfix b-info-container--home">
                                <div class="b-blog-one-column__info_container">
                                    <div class="b-blog-one-column__info">
                                        @if($artisan->level === 2)
                                        <i class="fa fa-phone"></i> {{$artisan->telephone}}
                                        @else
                                        <i class="fa fa-phone"></i> xxx xxxx xxx
                                        @endif
                                    </div>
                                    <div class="f-footer-mini-right f-selection">
                                        <i class="fa fa-map-marker"></i> {{$artisan->city}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            {!! $artisans->appends(Request::except('page'))->render() !!}
        </div>
    </div>


</div>
@endsection()
