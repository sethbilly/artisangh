@extends('layouts.public')

@section('slider')
@stop()

@section('main-content')


<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">

    </div>
    <div class="l-inner-page-container">
        <div class="container">

            <div class="row">
                <div class="col-md-9 col-md-push-3">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="b-employee-item b-employee-item--left b-employee-item--color">
                                <div class="view view-sixth" >
                                    @if(isset($member_detial->member_picture))
                                    <img data-retina="" src="{{asset('public/'.$member_detial->member_picture)}}" class="j-data-element animated fadeInDown"
                                         width="268" data-animate="fadeInDown">
                                    @else
                                    <img data-retina="" src="{{asset('public/index/wizard/imgs/default-avatar.png')}}" class="j-data-element animated fadeInDown"
                                         width="268" data-animate="fadeInDown">
                                    @endif
                                </div>
                                <h4 class="f-primary-b">{!! $member_detial->name !!}</h4>
                                <p>
                                    Location: {!! $member_detial->city !!} - {!! $member_detial->town !!}<br>
                                    Region: {!! $member_detial->region !!}<br>
                                    Telephone: {!! $member_detial->telephone !!}
                                </p>

                            </div>


                        </div>
                        <div class="col-sm-8">
                            <div class="b-article-box">
                                <div class="f-primary-l f-primary-title b-primary-title"></div>
                                <div class="b-portfolio_info_rating">


                                    <span class="b-rating_bord hidden-xs"></span>
                                    <div class="b-portfolio_rating_category">
                                        <span class="f-uppercase c-default f-primary-b">Profile</span>
                                    </div>
                                </div>
                                <div class="b-article__description">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="b-article-box">
                        <div class="b-slider b-slider--innerbullet">
                            <div class="flexslider flexslider-zoom">
                                <ul class="slides">
                                    @if(isset($works))
                                    @foreach($works as $work)
                                    <li>
                                        <img data-retina src="{{asset('public/'.$work->work_image)}}">
                                    </li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                            <div class="flexslider flexslider-thumbnail carousel-md">
                                <ul class="slides">

                                </ul>
                            </div>
                        </div>


                        <div class="b-tabs f-tabs j-tabs b-tabs-reset b-tabs--secondary f-tabs--secondary b-tab-top-search-container">
                            <div class="tabs-wrap">
                                <div class="j-tabs-btn-prev j-tabs-btns"> < </div>
                                <div class="j-tabs-btn-next j-tabs-btns"> > </div>
                                <ul class="j-tabs-check-size">
                                    <li><a href="#tabs-31">Location</a></li>
                                    <li><a href="#tabs-32">Contact Artisan</a></li>
                                </ul>
                            </div>


                            <div id="tabs-31" class="clearfix">

                                <div class="b-tab-top-search">
                                    <div id="map_canvas" style="width: 100%; height: 480px;">

                                    </div>
                                </div>
                            </div>

                            <div id="tabs-32" class="clearfix">
                                <div class="b-tab-top-search">
                                    <div class="b-contact-form-box--bord">
                                        <h3 class="f-primary-b b-title-description f-title-description">
                                            Contact agent
                                        </h3>
                                        <div class="row">
                                            <form action="#" method="post">
                                                <div class="col-md-6">
                                                    <div class="b-form-row">
                                                        <label class="b-form-vertical__label" for="name">Your name</label>
                                                        <div class="b-form-vertical__input">
                                                            <input type="text" id="name" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="b-form-row">
                                                        <label class="b-form-vertical__label" for="email">You email</label>
                                                        <div class="b-form-vertical__input">
                                                            <input type="text" id="email" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="b-form-row">
                                                        <label class="b-form-vertical__label" for="website">Your Contact</label>
                                                        <div class="b-form-vertical__input">
                                                            <input type="text" id="website" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="b-form-row b-form--contact-size">
                                                        <label class="b-form-vertical__label">Your message</label>
                                                        <textarea class="form-control" rows="4"></textarea>
                                                    </div>
                                                    <div class="b-form-row">
                                                        <a href="#" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100">send message</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="visible-xs-block visible-sm-block b-hr"></div>
                <div class="col-md-3 col-md-pull-9">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="b-find-box">
                                <div class="b-find_title f-find_title f-primary-b">
                                    <i class="fa fa-search"></i>
                                    Find Your Artisans
                                </div>
                                {!! Form::open(array('route'=>'search.artisans', 'method' =>'GET')) !!}
                                <div class="b-find_form f-find_form">
                                    <div class="b-form-row">
                                        <div class="row b-find--row">
                                            <div class="col-lg-12">
                                                <label class="b-form-vertical__label">Category</label>
                                                <div class="b-form-control__icon-wrap">
                                                    {!! Form::select('category', [''=> 'Select Category'] + $categories, null , array('class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label class="b-form-vertical__label">Region</label>
                                                <div class="b-form-control__icon-wrap">
                                                    {!! Form::select('region', [''=> 'Select Region'] + $regions, null , array('class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row b-find--row">
                                            <div class="col-lg-6">
                                                <input placeholder="City" class="form-control" name="city"/>
                                            </div>
                                            <div class="col-lg-6">
                                                <input placeholder="Town" class="form-control" name="town"/>
                                            </div>
                                        </div>
                                        <div class="b-form-horizontal__input">
                                            <button class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100 f-left" type="submit">search artisans</button>
                                        </div>
                                    </div>
                                </div>
                               {!! Form::close() !!}
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop()