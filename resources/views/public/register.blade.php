<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Register</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="{{asset('public/index/wizard/css/bootstrap.min.css')}}" rel="stylesheet" >
        <link href="{{asset('public/index/wizard/css/gsdk-base.css')}}" rel="stylesheet" >

        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    </head>

    <body>
        <div class="image-container set-full-height" style="background-image: url({{URL::asset('public/index/wizard/imgs/register_bg.jpg')}})">
            <!--   Big container   -->
            <div class="container">
                <div class="row">
                    @include('flash::message')
                    <div class="col-sm-8 col-sm-offset-2">

                        <!--      Wizard container        -->   
                        <div class="wizard-container"> 

                            <div class="card wizard-card ct-wizard-orange" id="wizardProfile">
                                {!! Form::open(array('route' => 'register-new', 'files'=>true)) !!}

                                <!--        You can switch "ct-wizard-orange"  with one of the next bright colors: "ct-wizard-blue", "ct-wizard-green", "ct-wizard-orange", "ct-wizard-red"             -->
                                <input type="hidden" name="_token" value="{{{csrf_token()}}}" />
                                <div class="wizard-header">
                                    <h3>
                                        <b>BUILD</b> YOUR PROFILE <br>
                                        <small>This information will let us know more about you.</small>
                                    </h3>
                                </div>
                                <ul>
                                    <li><a href="#account" data-toggle="tab">Account</a></li>
                                    <li><a href="#about" data-toggle="tab">Personal Info</a></li>
                                    <li><a href="#address" data-toggle="tab">Address</a></li>
                                    <li><a href="#skillset" data-toggle="tab">Skillset</a></li>
                                </ul>
                                <div class="tab-content">

                                    <div class="tab-pane" id="account">
                                        <h4 class="info-text"> Who are you?</h4>
                                        <label for="client"></label>
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-2">
                                                <div class="col-sm-3">

                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <input type="checkbox" name="client" value="artisan" >
                                                        <div class="icon">
                                                            <i class="fa fa-pencil"></i>
                                                        </div>
                                                        <h6>Artisan</h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <input type="checkbox" name="client" value="client" >
                                                        <div class="icon">
                                                            <i class="fa fa-user-secret"></i>
                                                        </div>
                                                        <h6>Client</h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="about">
                                        <div class="row">
                                            <h4 class="info-text"> Let's start with the basic information</h4>
                                            <div class="col-sm-4 col-sm-offset-1">
                                                <div class="picture-container">
                                                    <div class="picture">
                                                        <img src="{{asset('public/index/wizard/imgs/default-avatar.png')}}" class="picture-src" id="wizardPicturePreview" title=""/>
                                                        <input type="file" id="wizard-picture" name="memberPicture">
                                                    </div>
                                                    <h6>Choose Picture</h6>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input name="name" id="name" type="text" id="name" class="form-control" placeholder="Full name or company name.....">
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input name="email" type="email" class="form-control" placeholder="demo@demo.com....">
                                                </div>
                                                <div class="form-group">
                                                    <label for="telephone">Telephone <small>(required)</small></label>
                                                    <input name="telephone" id="telephone" type="text" class="form-control" placeholder="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="telephone1">Telephone1</label>
                                                    <input name="telephone1" id="telephone1" type="text" class="form-control" placeholder="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="address">
                                        <h4 class="info-text"> Where are you?</h4>
                                        <div class="row">
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="form-group">
                                                    <label>Region</label>
                                                    {!! Form::select('region', [''=> 'Select Region'] + $regions, null , array('class' => 'form-control', 'id'=>'region')) !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label>Town</label>
                                                    <input name="town" type="text" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <div class="City">
                                                    <label>City</label>
                                                    <input name="city" type="text" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <textarea name="address" type="text" class="form-control" placeholder=""></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="skillset">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 class="info-text">What are your skillset or interested skillset? </h4>
                                            </div>
                                            <div class="col-sm-7 col-sm-offset-1">
                                                <div class="checkbox">
                                                    <div class="row">
                                                        @foreach($categories as $cat)
                                                        <div class="col-sm-2 col-sm-offset-1">
                                                            <input type="checkbox" id ="category-name" name="categoryName[]" value="{{$cat->id}}">{{$cat->category_name}}
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="form-inline col-sm-6">
                                                <div class="member_pack">
                                                    <label>Member Package</label>
                                                    {!! Form::select('level', $levels, null , array('class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <br/>
                                    <div class="wizard-footer">
                                        <div class="pull-right">
                                            <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' name='next' value='Next' />
                                            <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' />
                                           
                                        </div>

                                        <div class="pull-left">
                                            
                                            <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
                                            <a href="{{URL::route('home')}}" style="margin-left: 200px;" class="btn btn-danger btn-sm">Cancel</a>
                                        </div>
                                        <div class="clearfix"></div> <div class="clearfix"></div>
                                    </div>	
                                    {!! Form::close() !!}
                                </div>
                            </div> <!-- wizard container -->
                        </div>
                    </div><!-- end row -->
                </div> <!--  big container -->

                <div class="footer">
                    <div class="container">

                    </div>
                </div>


            </div>

    </body>

    <script src="{{asset('public/index/wizard/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
    <script src="{{asset('public/index/wizard/js/jquery.validate.min.js')}}"></script><!--

      methods for manipulating the wizard and the validation 
-->    <script src="{{asset('public/index/wizard/js/wizard.js')}}"></script>
    <script src="{{asset('public/index/wizard/js/bootstrap.min.js')}}" type="text/javascript"></script>

    <!--   plugins 	 -->
    <script src="{{asset('public/index/wizard/js/jquery.bootstrap.wizard.js')}}" type="text/javascript"></script>


</html>