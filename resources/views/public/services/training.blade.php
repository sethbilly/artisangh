@extends('layouts.public')

@section('slider')
@stop()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Services</span></li>
                <li><i class="fa fa-angle-right"></i><span>Training</span></li>
            </ul>
        </div>
    </div>
    
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">training</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 animated animate-flip">
                    <img alt="certification" src="{{asset('public/index/images/services/training.png')}}"/>
                </div>
                
                <div class="col-md-8 text-justify">
                    <p>
                       AAG works with NVTI, COTVET and CIPS to deliver training to its members and other artisans and to help them achieve recognised certification in their field. We also train members in general areas of preparation of cost estimation, health and safety, customer service for artisans, workshop administration, effective site supervision, finished, etc.
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
@stop()
