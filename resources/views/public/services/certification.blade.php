@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Services</span></li>
                <li><i class="fa fa-angle-right"></i><span>Certification</span></li>
            </ul>
        </div>
    </div>
    
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">certification</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 animated animate-flip">
                    <img alt="certification" src="{{asset('public/index/images/services/seal.png')}}"/>
                </div>
                
                <div class="col-md-8 text-justify">
                    <p>
                        AAG has established a uniform system of engagement for apprentices. The program includes both theoretical and practical trainings. This is to ensure that they are well equipped to deliver as at when the need arises.
                    </p>
                    
                </div>
            </div>
        </div>
    </section>
</div>
@endsection()
