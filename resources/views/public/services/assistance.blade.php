@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Services</span></li>
                <li><i class="fa fa-angle-right"></i><span>Assistance</span></li>
            </ul>
        </div>
    </div>
    
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">assistance</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 animated animate-flip">
                    <img alt="certification" src="{{asset('public/index/images/services/assist.png')}}"/>
                </div>
                
                <div class="col-md-8 col-sm-offset-1 text-justify">
                    <p>
                        The Artisans Association Of Ghana assists artisans in the following ways:
                    </p>
                    <p>
                        <i class="fa fa-tag"></i>Easy access to NHIS registration.
                    </p>
                    <p>
                        <i class="fa fa-tag"></i>Group Insurance on Personal accidents.
                    </p>
                    <p>
                        <i class="fa fa-tag"></i>Facilitates the informal enrollment unto Social Security and National Insurance Trust.
                    </p>
                    <p>
                        <i class="fa fa-tag"></i>Access to legal aids.
                    </p>
                    <p>
                        These assistance are aimed at making the lives of artisans better and more fulfilling.
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection()