@extends('layouts.public')

@section('slider')
@endsection()

@section('main-content')
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{URL::route('home')}}"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span>Services</span></li>
                <li><i class="fa fa-angle-right"></i><span>Job Placement</span></li>
            </ul>
        </div>
    </div>
    
    <section class="b-desc-section-container b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-center f-primary-b f-legacy-h2">job placement</h2>
            <div class="b-hr-stars f-hr-stars">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 animated animate-flip">
                    <img alt="certification" src="{{asset('public/index/images/services/job.png')}}"/>
                </div>
                
                <div class="col-md-8 col-sm-offset-1 text-justify">
                    <p>
                        To increase exposure to job and apprenticeship opportunities, AAG provides a menu of services to its members. Job placement services include maintaining a list of members’ skills and expertise and matching the artisans with opportunities. AAG also submits proposals to construction firms and other private clients on behalf of its members. AAG's website allows potential clients to search the profile of its members.
                    </p>
                    
                </div>
            </div>
        </div>
    </section>
</div>
@endsection()
