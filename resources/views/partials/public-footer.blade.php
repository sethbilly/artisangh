
<footer>
    <div class="container">
        <div class="b-footer-secondary row">

            <div class="col-md-4 col-sm-12 col-xs-12">
                <h4 class="f-primary-b f-legacy-h4">Our Partners</h4>
                <div class="clearfix"></div>
                <ul class="rslides">
                    <li><img src="{{asset('public/index/images/logos/globalc.png')}}" alt=""></li>
                   <li><img src="{{asset('public/index/images/logos/aabn.png')}}" alt=""></li>
                   <li><img src="{{asset('public/index/images/logos/oicg.png')}}" alt=""></li>
                   <li><img src="{{asset('public/index/images/logos/yesgh.png')}}" alt=""></li>
                   <li><img src="{{asset('public/index/images/logos/hfcboafo.png')}}" alt=""></li>
               </ul>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <h4 class="f-primary-b f-legacy-h4">contact info</h4>
                <div class="b-contacts-short-item-group">
                    <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
                        <div class="b-contacts-short-item__icon f-contacts-short-item__icon f-contacts-short-item__icon_lg b-left">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="b-remaining f-contacts-short-item__text">
                            Artisans Association Of Ghana<br/>
                            H/No.# 3 Bethlehem,
                            Opposite Ho Station
                            Off the Michel Camp Road<br/>
                            Ghana.<br/>
                        </div>
                    </div>
                    <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
                        <div class="b-contacts-short-item__icon f-contacts-short-item__icon b-left f-contacts-short-item__icon_md">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="b-remaining f-contacts-short-item__text f-contacts-short-item__text_phone">
                            (030)3317234
                        </div>
                    </div>
                    <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
                        <div class="b-contacts-short-item__icon f-contacts-short-item__icon b-left f-contacts-short-item__icon_xs">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <div class="b-remaining f-contacts-short-item__text f-contacts-short-item__text_email">
                            <a href="mailto:info@artisansghana.org">info@artisansghana.org</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 ">
                <img src="" height="62" width="62" alt="an image" class="tooltip" title="Hello a tooltip">
                <h4 class="f-primary-b f-legacy-h4">Premium Artisans</h4>
                <div class="b-short-photo-items-group">
                    @foreach($premium_members as $pmember)
                    <div class="col-sm-3">
                        @if(isset($pmember->member_picture))
                        <a href="{{URL::route('artisan.detail', $pmember->id)}}" >
                            <img data-retina="" src="{{asset('public/'.$pmember->member_picture)}}" height="62" width="62" data-toggle="tooltip" title="{!! $pmember->name !!} Skill-{!! $pmember->skills->category_name !!} Please click image for more details">
                        </a>
                        @else
                        <a href="{{URL::route('artisan.detail', $pmember->id)}}" >
                            <img  src="{{asset('public/index/wizard/imgs/default-avatar.png')}}" height="62" width="62" data-toggle="tooltip" title="{!! $pmember->name !!} Skill-{!! $pmember->skills->category_name !!} Please click image for more details">

                        </a>
                        @endif
                    </div>
                    @endforeach()
                </div>
                {!! $premium_members->appends(Request::except('page'))->render() !!}
            </div>
        </div>
    </div>
    <div class="b-footer-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-xs-12 f-copyright b-copyright">Copyright © 2015 - All Rights Reserved</div>
                <div class="col-sm-8 col-xs-12">
                    <div class="b-btn f-btn b-btn-default b-right b-footer__btn_up f-footer__btn_up j-footer__btn_up">
                        <i class="fa fa-chevron-up"></i>
                    </div>
                    <nav class="b-bottom-nav f-bottom-nav b-right hidden-xs">
                        <ul>
                            <li><a href="{{URL::route('faqs')}}">FAQs</a></li>
                            <li ><a href="#">Disclaimer</a></li>
                            <li><a href="#">Terms and Conditions</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{asset('public/index/js/jquery/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('public/index/js/breakpoints.js')}}"></script>
<script src="{{asset('public/index/js/scrollbox/jquery.scrollbox.js')}}"></script>
<script src="{{asset('public/index/js/bootstrap.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('public/index/js/scrollspy.js')}}"></script>
<script src="{{asset('public/index/js/bootstrap-progressbar/bootstrap-progressbar.js')}}"></script>
<script src="{{asset('public/index/js/bootstrap.min.js')}}"></script>
<!-- end bootstrap -->
<script src="{{asset('public/index/js/masonry.pkgd.min.js')}}"></script>
<script src="{{asset('public/index/js/imagesloaded.pkgd.min.js')}}"></script>
<!-- bxslider -->
<script src="{{asset('public/index/js/bxslider/jquery.bxslider.min.js')}}"></script>
<!-- end bxslider -->
<!-- flexslider -->
<script src="{{asset('public/index/js/flexslider/jquery.flexslider.js')}}"></script>
<!-- end flexslider -->
<!-- smooth-scroll -->
<script src="{{asset('public/index/js/smooth-scroll/SmoothScroll.js')}}"></script>
<!-- end smooth-scroll -->
<!-- carousel -->
<script src="{{asset('public/index/js/jquery.carouFredSel-6.2.1-packed.js')}}"></script>
<!-- end carousel -->
<script src="{{asset('public/index/js/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
<script src="{{asset('public/index/js/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
<!--<script src="{{asset('public/index/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('public/index/js/jquery.themepunch.revolution.min.js')}}"></script>-->
<script src="{{asset('public/index/js/rs-plugin/videojs/video.js')}}"></script>

<!-- jquery ui -->
<script src="{{asset('public/index/js/jqueryui/jquery-ui.js')}}"></script>
<!-- end jquery ui -->

<!-- Modules -->
<script src="{{asset('public/index/js/modules/sliders.js')}}"></script>
<script src="{{asset('public/index/js/modules/ui.js')}}"></script>
<script src="{{asset('public/index/js/modules/retina.js')}}"></script>
<script src="{{asset('public/index/js/modules/animate-numbers.js')}}"></script>
<script src="{{asset('public/index/js/modules/parallax-effect.js')}}"></script>
<script src="{{asset('public/index/js/modules/settings.js')}}"></script>
<script src="{{asset('public/index/js/modules/maps-google.js')}}"></script>
<script src="{{asset('public/index/js/modules/color-themes.js')}}"></script>
<!-- End Modules -->

<!-- Audio player -->
<!--<script type="text/javascript" src="js/audioplayer/js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="js/audioplayer/js/jplayer.playlist.min.js"></script>
<script src="js/audioplayer.js"></script>-->
<!-- end Audio player -->

<!-- radial Progress -->
<script src="{{asset('public/index/js/radial-progress/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('public/index/js/d3.min.js')}}"></script>
<script src="{{asset('public/index/js/radial-progress/radialProgress.js')}}"></script>
<script src="{{asset('public/index/js/progressbars.js')}}"></script>
<!-- end Progress -->

<!-- Google services -->
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>
<script src="{{asset('public/index/js/google-chart.js')}}"></script>
<script src="{{asset('public/index/js/j.placeholder.js')}}"></script>

<!-- Fancybox -->
<script src="{{asset('public/index/js/fancybox/jquery.fancybox.pack.js')}}"></script>
<script src="{{asset('public/index/js/fancybox/jquery.mousewheel.pack.js')}}"></script>
<script src="{{asset('public/index/js/fancybox/jquery.fancybox.custom.js')}}"></script>
<!-- End Fancybox -->
<script src="{{asset('public/index/js/user.js')}}"></script>
<script src="{{asset('public/index/js/timeline.js')}}"></script>
<script src="{{asset('public/index/js/fontawesome-markers.js')}}"></script>
<script src="{{asset('public/index/js/markerwithlabel.js')}}"></script>
<script src="{{asset('public/index/js/cookie.js')}}"></script>
<script src="{{asset('public/index/js/loader.js')}}"></script>
<script src="{{asset('public/index/js/scrollIt/scrollIt.min.js')}}"></script>
<script src="{{asset('public/index/js/modules/navigation-slide.js')}}"></script>
<!-- Responsive Slide -->
<script src="{{asset('public/index/js/responsiveslide/responsiveslides.js')}}"></script>
<script>
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

$('#advert').scrollbox({
    infiniteLoop: false,
    direction: 'v',
    switchItems: 1,
    delay: 0,
    speed: 500,
    autoPlay: true,
    onMouseOverPause: true
});

$('#events').scrollbox({
    direction: 'h'
});

$(function () {
    $(".rslides").responsiveSlides();
});
</script>

@yield('wizard-js')

</body>

<!-- Mirrored from www.sungeetheme.com/html/page_header_9.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 15 Oct 2015 12:46:48 GMT -->
</html>
