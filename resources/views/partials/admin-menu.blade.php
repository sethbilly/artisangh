<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu">
                    <li class="site-menu-category">Dashboard</li>
                    <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon wb-layout" aria-hidden="true"></i>
                            <span class="site-menu-title">Membership</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link" href="{{URL::route('members')}}">
                                    <span class="site-menu-title">Members</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="{{URL::route('packages')}}">
                                    <span class="site-menu-title">Membership Level</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="site-menu-item has-sub">
                        <a href="{{URL::route('artisan-category')}}">
                            <i class="site-menu-icon wb-bookmark" aria-hidden="true"></i>
                            <span class="site-menu-title">Artisan Categories</span>
                        </a>
                    </li>
                    
                    <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon wb-file" aria-hidden="true"></i>
                            <span class="site-menu-title">News</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a href="{{URL::route('news')}}">
                                    <span class="site-menu-title">News Item</span>
                                </a>
                            </li>    

                            <li class="site-menu-item">
                                <a class="animsition-link" href="{{URL::route('events')}}">
                                    <span class="site-menu-title">Events</span>
                                </a>
                            </li>
                        </ul>     
                    </li>
                    <li class="site-menu-item has-sub">
                        <a href="{{URL::route('staff')}}">
                            <i class="site-menu-icon wb-bookmark" aria-hidden="true"></i>
                            <span class="site-menu-title">Staff</span>
                        </a>
                    </li>
                    <li class="site-menu-item has-sub">
                        <a href="{{URL::route('users')}}">
                            <i class="site-menu-icon wb-bookmark" aria-hidden="true"></i>
                            <span class="site-menu-title">Users</span>
                        </a>
                    </li>
                    <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon wb-bookmark" aria-hidden="true"></i>
                            <span class="site-menu-title">Media</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a href="{{URL::route('portfolios')}}">
                                    <span class="site-menu-title">Gallery</span>
                                </a>
                            </li> 
                            <li class="site-menu-item">
                                <a href="{{URL::route('adverts')}}">
                                    <span class="site-menu-title">Adverts</span>
                                </a>
                            </li>   
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </div>
    </div>

    <div class="site-menubar-footer">
        <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
           data-original-title="Settings">
            <span class="icon wb-settings" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
            <span class="icon wb-eye-close" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
            <span class="icon wb-power" aria-hidden="true"></span>
        </a>
    </div>
</div>
