<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="bootstrap public/admin template">
        <meta name="author" content="">

        <title>@yield('title')</title>

        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{asset('public/admin/global/css/bootstrap.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/global/css/bootstrap-extend.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/assets/css/site.min081a.css?v2.0.0')}}">

        <!--   Skin tools (demo site only) 
          <link rel="stylesheet" href="../global/css/skintools.min081a.css?v2.0.0">
          <script src="assets/js/sections/skintools.min.js"></script>-->

        <!-- Plugins -->
        <link rel="stylesheet" href="{{asset('public/admin/global/vendor/animsition/animsition.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/global/vendor/asscrollable/asScrollable.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/global/vendor/switchery/switchery.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/global/vendor/intro-js/introjs.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/global/vendor/slidepanel/slidePanel.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/global/vendor/flag-icon-css/flag-icon.min081a.css?v2.0.0')}}">

        <!-- Plugins For This Page -->
        <link rel="stylesheet" href="{{asset('public/admin/global/vendor/chartist-js/chartist.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/global/vendor/jvectormap/jquery-jvectormap.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/assets/examples/css/forms/layouts.min081a.css?v2.0.0')}}">

        <!-- Fonts -->
        <link rel="stylesheet" href="{{asset('public/admin/global/fonts/web-icons/web-icons.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="{{asset('public/admin/global/fonts/brand-icons/brand-icons.min081a.css?v2.0.0')}}">
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

        <link rel="stylesheet" href="{{asset('public/admin/global/fonts/weather-icons/weather-icons.min081a.css?v2.0.0')}}">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{asset('public/index/js/dropzone/dropzone.css')}}">

        <!--[if lt IE 9]>
          <script src="../global/vendor/html5shiv/html5shiv.min.js"></script>
          <![endif]-->

        <!--[if lt IE 10]>
          <script src="../global/vendor/media-match/media.match.min.js"></script>
          <script src="../global/vendor/respond/respond.min.js"></script>
          <![endif]-->

        <!-- Scripts -->
        <script src="{{asset('public/admin/global/vendor/modernizr/modernizr.min.js')}}"></script>
        <script src="{{asset('public/admin/global/vendor/breakpoints/breakpoints.min.js')}}"></script>
        
        <script>
            Breakpoints();
        </script> 
        @yield('tinymce')
    </head>
