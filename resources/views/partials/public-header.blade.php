<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <link rel="shortcut icon" href="{{asset('public/index/images/aag_icon.ico')}}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <link href="{{asset('public/index/css/hover-min.css')}}" rel="stylesheet" >
        <link href="{{asset('public/index/css/tooltip.css')}}" rel="stylesheet">
        <link href="{{asset('public/index/css/font-awesome.min.css')}}" rel="stylesheet">
        <!-- bxslider -->
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/bxslider/jquery.bxslider.css')}}">
        <!-- End bxslider -->
        <!-- flexslider --> 
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/flexslider/flexslider.css')}}">
        <!-- End flexslider -->

        <!-- bxslider -->
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/radial-progress/style.css')}}">
        <!-- End bxslider -->

        <!-- Animate css -->
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/css/animate.css')}}">
        <!-- End Animate css -->

        <!-- Bootstrap css -->
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/css/bootstrap.min.css')}}">
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/bootstrap-progressbar/bootstrap-progressbar-3.2.0.min.css')}}">
        <!-- End Bootstrap css -->

        <!-- Jquery UI css -->
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/jqueryui/jquery-ui.css')}}">
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/jqueryui/jquery-ui.structure.css')}}">
        <!-- End Jquery UI css -->

        <!-- Fancybox -->
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/fancybox/jquery.fancybox.css')}}">
        <!-- End Fancybox -->
        
        <!-- ResponsiveSlides -->
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/css/responsiveslides.css')}}">
        <!-- End ResponsiveSlides -->

        <link type="text/css" rel='stylesheet' href="{{asset('public/index/fonts/fonts.css')}}">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

        <link type="text/css" data-themecolor="default" rel='stylesheet' href="{{asset('public/index/css/main-default.css')}}">
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/rs-plugin/css/settings.css')}}">
        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/rs-plugin/css/settings-custom.css')}}">

        <link type="text/css" rel='stylesheet' href="{{asset('public/index/js/rs-plugin/videojs/video-js.css')}}">
        <script type="text/javascript" 
        src="https://maps.googleapis.com/maps/api/js"></script>
        <script type="text/javascript">
        var geocoder;
        var map;
        var address = "San Diego, CA";

        function initialize() {
            console.log('initializing map....................');
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var myOptions = {
                zoom: 8,
                center: latlng,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                },
                navigationControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            if (geocoder) {
                geocoder.geocode({
                    'address': address
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                            map.setCenter(results[0].geometry.location);

                            var infowindow = new google.maps.InfoWindow({
                                content: '<b>' + address + '</b>',
                                size: new google.maps.Size(150, 50)
                            });

                            var marker = new google.maps.Marker({
                                position: results[0].geometry.location,
                                map: map,
                                title: address
                            });
                            google.maps.event.addListener(marker, 'click', function () {
                                infowindow.open(map, marker);
                            });

                        } else {
                            alert("No results found");
                        }
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
            }
        }
        google.maps.event.addDomListener(window, 'load', initialize);
        </script>

    </head>
    <body onload="initialize()">
        <header class="b-header--bottom-menu">
            <div class="b-top-options-panel">
                <div class="container">
                    <div class="b-right">
                        <span class="b-option-contacts f-option-contacts">
                            <a href="{{URL::route('register')}}">Register</a>
                            <a href="{{URL::route('member-packages')}}">Membership</a>
                            <a href="{{URL::route('search.artisans')}}"><i class="fa fa-search-plus"></i>Search Artisans</a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="container b-header__box b-relative b-header--hide">
                <a href="#" class="b-left b-logo"><img class="color-theme" data-retina src="{{asset('public/index/images/aag_logo.png')}}" alt="Logo" /></a>
                <h3 style="margin-left: 110px; margin-top: 40px; color: #e26228; font-size: 30px; font-weight: bolder">ARTISANS  ASSOCIATION OF GHANA</h3>
                <div class="b-top-nav-show-slide f-top-nav-show-slide j-top-nav-show-slide b-right"><i class="fa fa-align-justify"></i></div>
            </div>
            <div class="container b-relative">
                <div class="b-header-r">
                    <nav class="b-top-nav b-top-nav--bottom b-top-nav--bottom--icon f-top-nav j-top-nav b-top-nav--icon">
                        <ul class="b-top-nav__1level_wrap">
                            <li class="b-top-nav__1level f-top-nav__1level is-active-top-nav__1level f-primary-b"><a href="{{URL::route('home')}}"><i class="fa fa-home b-menu-1level-ico"></i>Home <span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>

                            </li>
                            <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                                <a href="#"><i class="fa fa-folder-open b-menu-1level-ico"></i>About Us<span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>
                                <div class="b-top-nav__dropdomn">
                                    <ul class="b-top-nav__2level_wrap">
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('who-we-are')}}"><i class="fa fa-angle-right"></i>Who We Are</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('vision')}}"><i class="fa fa-angle-right"></i>Vision</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('mission')}}"><i class="fa fa-angle-right"></i>Mission</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('objectives')}}"><i class="fa fa-angle-right"></i>Objectives</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('contact-us')}}"><i class="fa fa-angle-right"></i>Contact Us</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                                <a href="#"><i class="fa fa-picture-o b-menu-1level-ico"></i>Services<span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>
                                <div class="b-top-nav__dropdomn">
                                    <ul class="b-top-nav__2level_wrap">
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('certification')}}"><i class="fa fa-angle-right"></i>Certification</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('job-placement')}}"><i class="fa fa-angle-right"></i>Job Placement</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('training')}}"><i class="fa fa-angle-right"></i>Training</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('assistance')}}"><i class="fa fa-angle-right"></i>Assistance</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                                <a href="#"><i class="fa fa-code b-menu-1level-ico"></i>Partners<span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>
                                <div class="b-top-nav__dropdomn">
                                    <ul class="b-top-nav__2level_wrap">
                                        <li class="b-top-nav__2level_title f-top-nav__2level f-primary"><a href="{{URL::route('aabn')}}"><i class="fa fa-angle-right"></i>AABN</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('global-communities')}}"><i class="fa fa-angle-right"></i>Global Communities</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('hfc-boafo')}}"><i class="fa fa-angle-right"></i>HFC Boafo</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('yes-gh')}}"><i class="fa fa-angle-right"></i>YES GH</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('ocig')}}"><i class="fa fa-angle-right"></i>OICG</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('cotvet')}}"><i class="fa fa-angle-right"></i>COTVET</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('nvti')}}"><i class="fa fa-angle-right"></i>NVTI</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                                <a href="#"><i class="fa fa-cloud-download b-menu-1level-ico"></i>Programs<span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>
                                <div class="b-top-nav__dropdomn">
                                    <ul class="b-top-nav__2level_wrap">
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('yedie')}}"><i class="fa fa-angle-right"></i>YIEDIE</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                                <a href="#"><i class="fa fa-list b-menu-1level-ico"></i>Media Center <span class="b-ico-dropdown"><i class="fa fa-arrow-circle-down"></i></span></a>
                                <div class="b-top-nav__dropdomn">
                                    <ul class="b-top-nav__2level_wrap">
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('gallery')}}"><i class="fa fa-angle-right"></i>Gallery</a></li>
                                        <li class="b-top-nav__2level f-top-nav__2level f-primary"><a href="{{URL::route('news-events')}}"><i class="fa fa-angle-right"></i>News And Events</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
                                <a href="{{URL::route('partner-us')}}"><i class="fa fa-folder-open b-menu-1level-ico"></i>Partner Us</a>
                            </li>
                            <li class="b-top-nav__1level f-top-nav__1level f-primary-b b-top-nav-big">
                                <a href="{{URL::route('faqs')}}"><i class="fa fa-question-circle b-menu-1level-ico"></i>FAQS</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div class="j-menu-container"></div>
        <div class="l-main-container">
            @yield('slider')

            @yield('main-content')
        </div>