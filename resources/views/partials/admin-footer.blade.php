
<!-- Core  -->
<script src="{{asset('public/admin/global/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/animsition/animsition.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/asscroll/jquery-asScroll.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/mousewheel/jquery.mousewheel.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/asscrollable/jquery.asScrollable.all.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js')}}"></script>

<!-- Plugins -->
<script src="{{asset('public/admin/global/vendor/switchery/switchery.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/intro-js/intro.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/slidepanel/jquery-slidePanel.min.js')}}"></script>

<!-- Plugins For This Page -->
<script src="{{asset('public/admin/global/vendor/skycons/skycons.js')}}"></script>
<!--        <script src="{{asset('public/admin/global/vendor/chartist-js/chartist.min.js')}}"></script>-->
<script src="{{asset('public/admin/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/aspieprogress/jquery-asPieProgress.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/jvectormap/jquery.jvectormap.min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
<script src="{{asset('public/admin/global/vendor/jquery-placeholder/jquery.placeholder.min.js')}}"></script>


<!-- Scripts -->
<script src="{{asset('public/admin/global/js/core.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/site.min.js')}}"></script>

<script src="{{asset('public/admin/assets/js/sections/menu.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/sections/menubar.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/sections/gridmenu.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/sections/sidebar.min.js')}}"></script>

<script src="{{asset('public/admin/global/js/configs/config-colors.min.js')}}"></script>
<script src="{{asset('public/admin/assets/js/configs/config-tour.min.js')}}"></script>

<script src="{{asset('public/admin/global/js/components/asscrollable.min.js')}}"></script>
<script src="{{asset('public/admin/global/js/components/animsition.min.js')}}"></script>
<script src="{{asset('public/admin/global/js/components/slidepanel.min.js')}}"></script>
<script src="{{asset('public/admin/global/js/components/switchery.min.js')}}"></script>

<script src="{{asset('public/admin/global/js/components/matchheight.min.js')}}"></script>
<script src="{{asset('public/admin/global/js/components/jvectormap.min.js')}}"></script>


<script src="{{asset('public/admin/assets/examples/js/dashboard/v1.min.js')}}"></script>
<script src="{{asset('public/index/wizard/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('public/index/wizard/js/wizard.js')}}"></script>
<!--        <script src="//code.jquery.com/jquery-1.10.2.js"></script>-->
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{asset('public/index/js/dropzone/dropzone.js')}}"></script>

<script src="{{asset('public/index/js/dropzone/dropzone-config.js')}}"></script>
<script>
$("#newsDate").datepicker({dateFormat: 'yy-mm-dd'});
</script>
