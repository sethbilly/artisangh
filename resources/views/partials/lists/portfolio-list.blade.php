<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="example-wrap">
            <h4 class="example-title">Portfolios</h4>
            <div class="example">
                <table class="table-responsive table-bordered table-primary table-striped">
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                    @foreach($portfolioList as $portfolio)
                    <tr>
                        <td>{{$portfolio->title}}</td>
                        <td>{{$portfolio->desc}}</td>
                        <td>
                            <a href="{{URL::route('portfolio.edit', $portfolio->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                                <span class="icon wb-edit"></span>Edit
                            </a> |
                            <a href="{{URL::route('portfolio.delete', $portfolio->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                                <span class="icon wb-trash"></span>Delete
                            </a> |
                            <a href="{{URL::route('portfolio.addPhotos', $portfolio->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Add Photos">
                                <span class="icon wb-add-file"></span>Add Photos
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </table>
                {!! $portfolioList->render() !!}
            </div>
        </div>
    </div>
</div>
