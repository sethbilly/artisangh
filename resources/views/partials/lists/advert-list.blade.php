<div class="col-sm-12">
    <div class="example-wrap">
        <h4 class="example-title">Adverts</h4>
        <div class="example">
            <table class="table-responsive table-bordered table-primary table-striped">
                <tr>
                    <th>Title</th>
                    <th>Feature Image</th>
                    <th>Advert Date</th>
                    <th>Active</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                @foreach($advertList as $advert)
                <tr>
                    <td>{{$advert->title}}</td>
                    <td>{{$advert->feature_image}}</td>
                    <td>{{$advert->advert_date}}</td>
                    <td>{{$advert->published}}</td>
                    <td>
                        <a href="{{URL::route('advert.edit', $advert->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-edit"></span>
                        </a>
                    </td>
                    <td>
                         <a href="{{URL::route('advert.delete', $advert->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-trash"></span>
                        </a>
                    </td>
                </tr>
                @endforeach
            </table>
            {!! (new App\Pagination($advertList))->render() !!}
        </div>
    </div>
</div>
