<div class="col-sm-12">
    <!-- Example Basic Form Without Label -->
    <div class="example-wrap">
        <h4 class="example-title">User Accounts</h4>
        <div class="example">
            <!-- category table comes here -->
            <table class="table-responsive table-bordered table-striped">
                <tr>
                    <th>Name </th>
                    <th>Telephone</th>
                    <th>Email</th>
                    <th>Status</th>
                </tr>
                @forelse($userList as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->telephone}}</td>
                    <td>{{$user->emal}}</td>
                    <td>
                        <a href="{{URL::route('user.edit', $user->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-edit"></span>
                        </a>
                    </td>
                    <td>
                        <a href="{{URL::route('user.destroy', $user->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-trash"></span>
                        </a>
                    </td>
                </tr>
                @empty
                <p>No records found</p>
                @endforelse
            </table>
            {!! $userList->render() !!}
        </div>
    </div>
    <!-- End Example Basic Form Without Label -->
</div>