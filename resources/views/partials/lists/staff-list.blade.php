<div class="clearfix"></div>
<div class="col-sm-12">
    <!-- Example Basic Form Without Label -->
    <div class="example-wrap">
        <h4 class="example-title">Staff</h4>
        <div class="example">
            <!-- category table comes here -->
            <table class="table-responsive table-bordered table-striped">
                <tr>
                    <th>Name </th>
                    <th>Telephone</th>
                    <th>Email</th>
                    <th>Status</th>
                </tr>
                @forelse($staffList as $staff)
                <tr>
                    <td>{{$staff->name}}</td>
                    <td>{{$staff->telephone}}</td>
                    <td>{{$staff->emal}}</td>
                    <td>
                        <a href="{{URL::route('staff.edit', $staff->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-edit"></span>
                        </a>
                    </td>
                    <td>
                        <a href="{{URL::route('staff.destroy', $staff->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-trash"></span>
                        </a>
                    </td>
                </tr>
                @empty
                <p>No staff available</p>
                @endforelse
            </table>
        </div>
        {!! $staffList->render() !!}
    </div>
    <!-- End Example Basic Form Without Label -->
</div>
