<div class="col-sm-12">
    <!-- Example Basic Form Without Label -->

    <div class="example-wrap">
        <h4 class="example-title">Members</h4>
        
        {!! Form::open(array('route'=> 'member.filter', 'method'=>'GET')) !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group col-sm-2">
                    <label class="control-label" for="name">Name</label>
                    <input name="searchName" type="text" id="name" class="form-control" />
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label" for="category">Category</label>
                    {!! Form::select('searchCategory', [''=> 'Select Category'] + $categories, null , array('class'=>'form-control')) !!}
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label" for="region">Region</label>
                    {!! Form::select('searchRegion', [''=> 'Select Region'] + $regions, null , array('class'=>'form-control')) !!}
                </div>

                <div class="form-group col-sm-2">
                    <label class="control-label" for="city">City</label>
                    <input type="text" name="searchCity" id="city" class="form-control"/>
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label" for="town">Town</label>
                    <input type="text" name="serchTown" id="town" class="form-control"/>
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label" for="level">Level</label>
                    {!! Form::select('searchLevel', [''=> 'Select Level'] + $levels, null ,array('class'=>'form-control')) !!}
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group col-sm-2">
                    <label class="control-label" for="paid">Payment Made</label>
                    {!! Form::select('paid', [''=> 'Select Level'] + ['No', 'Yes'], null ,array('class'=>'form-control', 'id' =>'paid')) !!}
                </div>

                <div class="form-group col-sm-2">
                    <label class="control-label" for="btn-search"> </label>
                    <button class="btn btn-primary btn-sm" id="btn-search"><i class="fa fa-search"></i> Search</button>
                </div>
                {!! Form::token() !!}
                {!! Form::close() !!}

                <div class="form-group col-sm-2">
                    <label class="control-label" for="print"> </label>
                    <a id="print" href="#" class="btn btn-info btn-sm" ><i class="fa fa-print"></i> Print</a>
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label" for="export"> </label>
                    <a href="{{URL::route('member.export')}}" class="btn btn-info btn-sm" id="export"><i class="fa fa-expan"></i> Export To Excel</a>
                </div>

            </div>


        </div>
        <div class="example">
            <!-- category table comes here -->
            <h4 class="example-title">
                {{$memberList->total()}} recorded
            </h4>
            <table class="table-responsive table-bordered table-primary table-striped">
                <tr>
                    <th>Member Name</th>
                    <td>Picture</td>
                    <th>Member ID</th>
                    <th>Date Of Registration</th>
                    <th>Skillset</th>
                    <th>Level</th>
                    <th>Telephone</th>
                    <th>Region</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                @foreach($memberList as $member)
                <tr>
                    <td>{{$member->name}}</td>
                    <td>
                        @if(isset($member->member_picture))
                        <img src="{{asset('public/'.$member->member_picture)}}" height="60" width="60" class="thumbnail" alt=""/>
                        <a href="{{URL::route('member.removeImage', $member->id)}}" class="btn btn-danger btn-xs" >Remove Picture</a>
                        @else
                        <img src="{{asset('public/index/wizard/imgs/default-avatar.png')}}" height="60" width="60" class="thumbnail" alt=""/>
                        @endif
                    </td>
                    <td>{{$member->member_id}}</td>
                    <td>{{$member->dor}}</td>
                    <td>{{$member->skills->category_name}}</td>
                    <td>{{$member->memberlevel->level_name}}</td>
                    <td>{{$member->telephone}}</td>
                    <td>{{$member->region}}</td>
                    <td>
                        <a href="{{URL::route('member.edit', $member->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-edit"></span>
                        </a>
                    </td>
                    <td>
                        <a href="{{URL::route('member.destroy', $member->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-trash"></span>
                        </a>
                    </td>

                </tr>
                @endforeach
            </table>
            {!! $memberList->appends(Request::except('page'))->render() !!}
        </div>
        
    </div>
    <!-- End Example Basic Form Without Label -->

</div>