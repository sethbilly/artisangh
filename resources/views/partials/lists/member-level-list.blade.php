<div class="col-sm-6">
    <!-- Example Basic Form Without Label -->
    <div class="example-wrap">
        <h4 class="example-title">Membership Levels</h4>
        <div class="example">
            <table class="table-responsive table-bordered table-primary table-striped">
                <tr>
                    <th>Level Name</th>
                    <th>Level Price</th>
                    <th>Level Period</th>
                    <th>Active</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                @foreach($memberLevelList as $level)
                <tr>
                    <td>{{$level->level_name}}</td>
                    <td>{{$level->level_price}}</td>
                    <td>{{$level->level_period}}</td>
                    <td>{{$level->is_active}}</td>
                    <td>
                        <a href="{{URL::route('packages.edit', $level->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-edit"></span>
                        </a>
                    </td>
                    <td>
                         <a href="{{URL::route('packages.destroy', $level->id)}}" data-placement="top" data-toggle="tooltip" data-original-title="Delete">
                            <span class="icon wb-trash"></span>
                        </a>
                    </td>
                </tr>
                @endforeach
            </table>
            {!! (new App\Pagination($memberLevelList))->render() !!}
        </div>
    </div>
    <!-- End Example Basic Form Without Label -->
</div>