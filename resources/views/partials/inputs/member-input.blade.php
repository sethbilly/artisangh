
<div class="row">
    <div class="col-sm-6">
        <div class="picture">
            <img src="{{asset('public/index/wizard/imgs/default-avatar.png')}}" class="picture-src" id="wizardPicturePreview" title=""/>
            <input type="file" id="wizard-picture" name="memberPicture" value="{!! $member->member_picture !!}">
        </div>
        <h6>Choose Picture</h6>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label" for="name">Full Name</label>
            <input type="text" class="form-control" id="name" name="name"
                   placeholder="Full Name" value="{!! $member->name !!}"/>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label" for="email">Member ID</label>
            <input type="text" class="form-control" id="memberID" name="memberID"
                   placeholder="Member ID" value="{!! $member->member_id !!}"/>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label" for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email"
                   placeholder="Email" value="{!! $member->email !!}"/>
        </div>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label">Is client?</label>
        <input type="checkbox" name="isClient" value="{!! $member->is_client !!}"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="telephone">Telephone</label>
        <input type="text" class="form-control" id="levelPeriod" name="telephone"
               placeholder="Telephone" value="{!! $member->telephone !!}"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="telephone1">Telephone1</label>
        <input type="text" class="form-control" id="telephone" name="telephone1"
               placeholder="Telephone1" value="{!! $member->telephone1 !!}"/>
    </div>
     <div class="form-group col-sm-6">
        <label class="control-label" for="newsDate">Date Of Registration</label>
        <input type="date" name="newsDate" class="form-control" id="newsDate" value="{!! $member->dor !!}" placeholder="Date Of Registration"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="address">Address</label>
        <textarea class="form-control" id="address" name="address" rows="5"
                  placeholder="Enter Address" value="{!! $member->address !!}"></textarea>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="region">Region</label>
        {!! Form::select('region', $regions, null, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="town">Town</label>
        <input type="text" class="form-control" id="town" name="town"
               placeholder="Town" value="{!! $member->town !!}"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="city">City</label>
        <input type="text" class="form-control" id="city" name="city"
               placeholder="City" value="{!! $member->city !!}"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="city">Membership Level</label>
        {!! Form::select('level',  $levels, Input::old('level', $member->level), array('class'=>'form-control')) !!}
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="skills">Skills</label>
        {!! Form::select('category', $categories, Input::old('category', $member->skill_having) , array('class' => 'form-control', 'multiple' => true)) !!}
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="verify">Verfied ?</label>
       {!! Form::checkbox('verified', '0', Input::old('verified', $member->verified)) !!}
    </div>

</div>