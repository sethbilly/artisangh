
<div class="form-group col-sm-6">
    <label class="control-label" for="categoryName">Category Name</label>
    <input type="text" class="form-control" placeholder="Category Name" name="categoryName" value="{!! $artisanCategory->category_name !!}"
</div>
<div class="form-group col-sm-6">
    <label class="control-label" for="description">Description</label>
    <textarea class="form-control" id="description" name="description"
              placeholder="Enter Description" >
        {!! $artisanCategory->description !!}
    </textarea>
</div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
</div>