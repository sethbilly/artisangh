<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label" for="pwd">Temporary Password</label>
                    <input type="password" class="form-control" id="pwd" name="password"
                           placeholder="Password" value="{!! $user->password !!}"/>
                </div>
            </div>
            <div class="col-sm-6">
                <label class="control-label" for="user_status">Active ?</label>
                {!! Form::checkbox('user_status', '1', Input::old('user_status', $user->user_status)) !!}
            </div>
            <div class="col-sm-6">
                <label class="control-label" for="user_status">Super Admin ?</label>
                {!! Form::checkbox('super_user', '1', Input::old('super_user', $user->super_user)) !!}
            </div>
        </div>
        <div class="col-sm-6">

            <input type="hidden" class="form-control" id="name" name="name"
                   placeholder="Full Name" value="{!! $user->name !!}"/>
        </div>
        <div class="col-sm-6">
            <div class="form-group">

                <input type="hidden" class="form-control" id="email" name="email"
                       placeholder="Email" value="{!! $user->email !!}"/>
            </div>
        </div>
        <div class="form-group col-sm-6">

            <input type="hidden" class="form-control" id="levelPeriod" name="telephone"
                   placeholder="Telephone" value="{!! $user->telephone !!}"/>
        </div>
        <div class="form-group col-sm-6">
           
            <input type="hidden" class="form-control" id="telephone" name="telephone1"
                   placeholder="Telephone1" value="{!! $user->telephone1 !!}"/>
        </div>
        <div class="form-group col-sm-6">
           
            <input type="hidden" class="form-control" id="address" name="address"
                      placeholder="Enter Address" value="{!! $user->address !!}" />
        </div>
        <div class="form-group col-sm-6">
            <input type="hidden" class="form-control" id="address" name="address"
                      placeholder="Enter Address" value="{!! $user->user_status !!}" />
        </div>
    </div>
</div>