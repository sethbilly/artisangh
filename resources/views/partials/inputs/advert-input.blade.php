<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="control-label" for="title"></label>
            <input type="text" class="form-control" id="categoryName" name="title"
                   placeholder="Title" value="{!! $advert->title !!}"/>
        </div>
        <div class="form-group">
            <label class="control-label" for="body">Advert Date</label>
            <input type="date" name="advertDate" class="form-control datepicker" id="newsDate" value="{!! $advert->advert_date !!}" placeholder="Publish Date"/>
        </div>
        <div class="form-group ">
            <label class="control-label" for="body">Feature Image</label>
            <input type="file" name="featureImage" value="{!! $advert->feature_image !!}"/>
        </div>
        <div class="form-group">
            <label class="control-label" for="body">Publish</label>
            {!! Form::checkbox('published', '1', Input::old('published', $advert->published)) !!}
        </div>
    </div>

    <div class=" col-sm-8">
        <div class="form-group">
            <label class="control-label" for="desc">Advert Description</label>
            <textarea id="desc" name="desc">
            {!! $advert->description !!}
            </textarea>
        </div>
    </div>
</div>
