
<div class="row">
    <div class="form-group col-sm-6">
        <label class="control-label" for="levelName">Level Name</label>
        <input type="text" class="form-control" id="levelName" name="levelName"
               placeholder="Level Name" value="{!! $memberLevel->level_name !!}"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="description">Description</label>
        <textarea class="form-control" id="description" name="description"
                  placeholder="Enter Description" >
                      {!! $memberLevel->description !!}
        </textarea>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="levelPrice">Level Price</label>
        <input type="number" class="form-control" id="levelPrice" name="levelPrice"
               placeholder="Level Price" min="0" value="{!! $memberLevel->level_price !!}"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="levelPeriod">Level Period</label>
        <input type="text" class="form-control" id="levelPeriod" name="levelPeriod"
               placeholder="Level Period" value="{!! $memberLevel->level_period !!}"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="isActive">Active</label>
        {!! Form::checkbox('isActive', '1', $memberLevel->is_active) !!}
    </div>
</div>