
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="control-label" for="title"></label>
            <input type="text" class="form-control" id="categoryName" name="title"
                   placeholder="Title" value="{!! $news->title !!}"/>
        </div>
        <div class="form-group">
            <label class="control-label" for="body">News Date</label>
            <input type="date" name="newsDate" class="form-control datepicker" id="newsDate" value="{!! $news->news_date !!}" placeholder="Publish Date"/>
        </div>
        <div class="form-group ">
            <label class="control-label" for="body">Feature Image</label>
            <input type="file" name="featureImage" value="{!! $news->feature_images !!}"/>
        </div>
        <div class="form-group">
            <label class="control-label" for="body">Publish</label>
            {!! Form::checkbox('published', '1', Input::old('published', $news->publish)) !!}

        </div>
    </div>

    <div class="form-group col-sm-8">
        <label class="control-label" for="body">News Content</label>
        <textarea id="newsArea" name="body">
            {!! $news->body !!}
        </textarea>
    </div>
</div>