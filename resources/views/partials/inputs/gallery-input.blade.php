
<div class="row">
    <div class="col-lg-12">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label" for="name">Title</label>
                <input type="text" class="form-control" id="title" name="title"
                       placeholder="Title" value="{!! $gallery->title !!}" required />
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label" for="desc">Description</label>
                <input type="text" class="form-control" id="desc" name="desc"
                       placeholder="" value="{!! $gallery->desc !!}"/>
            </div>
        </div>
    </div>
    
</div>






