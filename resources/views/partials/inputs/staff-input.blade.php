<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label" for="name">Full Name</label>
            <input type="text" class="form-control" id="name" name="name"
                   placeholder="Full Name" value="{!! $staff->name !!}"/>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label" for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email"
                   placeholder="Email" value="{!! $staff->email !!}"/>
        </div>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="telephone">Telephone</label>
        <input type="text" class="form-control" id="levelPeriod" name="telephone"
               placeholder="Telephone" value="{!! $staff->telephone !!}"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="telephone1">Telephone1</label>
        <input type="text" class="form-control" id="telephone" name="telephone1"
               placeholder="Telephone1" value="{!! $staff->telephone1 !!}"/>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="address">Address</label>
        <textarea class="form-control" id="address" name="address" rows="5"
                  placeholder="Enter Address" value="{!! $staff->address !!}"></textarea>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label" for="verify">Active ?</label>
       {!! Form::checkbox('staff_status', '1', Input::old('staff_status', $staff->staff_status)) !!}
    </div>
</div>
