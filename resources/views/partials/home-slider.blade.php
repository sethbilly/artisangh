<div class="b-slidercontainer b-slider" style="height: 400px;">
    <div class="j-fullscreenslider" >
        <ul>
            <!-- Slide 1 -->
            <li data-transition="3dcurtain-vertical" data-slotamount="7" >
                <!-- Main Image -->
                <img data-retina src="{{asset('public/index/images/slider/slider_bg_new.png')}}">
                <!-- Layer 1 -->
                <div class="caption sfb"  data-x="70" data-y="20" data-speed="700" data-start="1700" data-easing="Power4.easeOut" style="z-index: 2">
                    <img data-retina src="{{asset('public/index/images/slider/slider_img1.jpg')}}">
                </div>
                <div class="caption sfr"  data-x="700" data-y="100" data-speed="600" data-start="2600">
                    <h1 class="f-primary-b c-white f-legacy-h1" >Join Us Today</h1>
                </div>
                <div class="caption"  data-x="800" data-y="170" data-speed="600" data-start="3200">
                    <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
                          <a href="{{URL::route('register')}}" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b">register</a>
                          <span class="b-slider-lg-info-l__item-link-after"><i class="fa fa-chevron-right"></i></span>
                      </div>
                </div>
                <div class="caption sfr" data-x="750" data-y="240" data-speed="600" data-start="3700">
                    <p class="f-primary-b f-slider-sm-item__text_desc c-white">
                        Join us today and enjoy the benefits
                    </p>
                </div>

            </li>
            <!-- Slide 2 -->
            <li data-transition="" data-slotamount="7">
                <div class="tp-bannertimer"></div>
                <img data-retina src="{{asset('public/index/images/slider/slider_bg_new.png')}}">
                <div class="caption sfb"  data-x="20" data-y="25" data-speed="700" data-start="1700" data-easing="Power4.easeOut">
                    <img data-retina src="{{asset('public/index/images/slider/slide_img2.jpg')}}">
                </div>
                <div class="caption sfr"  data-x="690" data-y="100" data-speed="600" data-start="2600">
                    <h1 class="f-primary-b c-white f-legacy-h1" >Vision</h1>
                </div>
                <div class="caption"  data-x="690" data-y="170" data-speed="600" data-start="3200">
                    <p class="f-primary-b f-slider-sm-item__text_desc c-white" >
                        At AGG we seek to reduce youth unemployment in the formal sector
                    </p>
                </div>
                <div class="caption"  data-x="690" data-y="250" data-speed="800" data-start="3600">
                    <a href="{{URL::route('vision')}}" class="button-sm">read more</a>
                </div>
            </li>
            <!-- Slide 3 -->
            <li data-transition="" data-slotamount="7">
                <div class="tp-bannertimer"></div>
                <img data-retina src="{{asset('public/index/images/slider/slider_bg_new.png')}}">
                <div class="caption sfb"  data-x="20" data-y="25" data-speed="700" data-start="1700" data-easing="Power4.easeOut">
                    <img data-retina src="{{asset('public/index/images/slider/slide_img3.jpg')}}">
                </div>
                
                <div class="caption sfr"  data-x="700" data-y="110" data-speed="600" data-start="2600">
                    <h1 class="f-primary-b c-white f-legacy-h1" >Mission</h1>
                </div>
                <div class="caption"  data-x="700" data-y="170" data-speed="600" data-start="3200">
                    <p class="f-primary-b f-slider-sm-item__text_desc c-white" >
                       The mission of AAG is to get over 20,000 youth employed<br /> in the informal sector by 2020.
                    </p>
                </div>
                <div class="caption"  data-x="700" data-y="250" data-speed="800" data-start="3600">
                    <a href="{{URL::route('mission')}}" class="button-sm">read more</a>
                </div>
            </li>
            <!-- Slide 4 -->
            <li data-transition="" data-slotamount="7">
                <div class="tp-bannertimer"></div>
                <img data-retina src="{{asset('public/index/images/slider/slider_bg_new.png')}}">
                <div class="caption sfb"  data-x="20" data-y="10" data-speed="700" data-start="1700" data-easing="Power4.easeOut">
                    <img data-retina src="{{asset('public/index/images/slider/slide_img4.jpg')}}">
                </div>
                
                <div class="caption lft"  data-x="700" data-y="100" data-speed="600" data-start="2600">
                    <h1 class="f-primary-b c-white f-legacy-h1" >Core Values</h1>
                </div>
                <div class="caption"  data-x="700" data-y="170" data-speed="600" data-start="3200">
                    <p class="f-primary-b f-slider-sm-item__text_desc c-white" >
                        Our organisation is build on Unity, Teamwork and Integrity.
                    </p>
                </div>
                <div class="caption"  data-x="700" data-y="250" data-speed="800" data-start="3600">
                    <a href="#" class="button-sm">read more</a>
                </div>
            </li>
            <!-- Slide 5 -->
            <li data-transition="" data-slotamount="7">
                <div class="tp-bannertimer"></div>
                <img data-retina src="{{asset('public/index/images/slider/slider_bg_new.png')}}">
                <div class="caption sfb"  data-x="20" data-y="20" data-speed="700" data-start="1700" data-easing="Power4.easeOut">
                   <img data-retina src="{{asset('public/index/images/slider/slide_img5.png')}}">
                </div>
                
                <div class="caption sfr"  data-x="700" data-y="100" data-speed="600" data-start="2600">
                    <h1 class="f-primary-b c-white f-legacy-h1" >Programmes</h1>
                    
                </div>
                <div class="caption"  data-x="700" data-y="170" data-speed="600" data-start="3200">
                    <p class="f-primary-b f-slider-sm-item__text_desc c-white" >
                        We run programmes that build youth capacity,<br /> and designed to create economic opportunities.
                    </p>
                </div>
                <div class="caption"  data-x="700" data-y="250" data-speed="800" data-start="3600">
                    <a href="{{URL::route('yedie')}}" class="button-sm">read more</a>
                </div>
            </li>
            <!-- Slide 6 -->
<!--            <li data-transition="" data-slotamount="7">
                <div class="tp-bannertimer"></div>
                <img data-retina src="{{asset('public/index/images/slider/slider_bg_new.png')}}">
                <div class="caption sfb"  data-x="10" data-y="60" data-speed="700" data-start="1700" data-easing="Power4.easeOut">
                   <img data-retina src="{{asset('public/index/images/slider/slide_img6.png')}}">
                </div>
                
                <div class="caption lft"  data-x="700" data-y="70" data-speed="600" data-start="2600">
                    <h1 class="f-primary-b c-white f-legacy-h1" >Partners</h1>
                </div>
                <div class="caption"  data-x="700" data-y="170" data-speed="600" data-start="3200">
                    <p class="f-primary-b f-slider-sm-item__text_desc c-white" >
                      We are affiliated to NVTI and COVET.
                    </p>
                </div>
                <div class="caption"  data-x="700" data-y="250" data-speed="800" data-start="3600">
                    <a href="{{URL::route('global-communities')}}" class="button-sm">read more</a>
                </div>
            </li>-->
        </ul>
    </div>
</div>